/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:30
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.enums;

public enum CMSPageOperation
{

	/** <i>Generated enum value</i> for <code>CMSPageOperation.TRASH_PAGE</code> value defined at extension <code>cmsfacades</code>. */
	TRASH_PAGE  

}
