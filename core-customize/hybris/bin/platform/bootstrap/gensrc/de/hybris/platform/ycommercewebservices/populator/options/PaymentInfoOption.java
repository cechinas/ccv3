/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:25
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ycommercewebservices.populator.options;

public enum PaymentInfoOption
{

	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BASIC</code> value defined at extension <code>ycommercewebservices</code>. */
	BASIC , 
	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BILLING_ADDRESS</code> value defined at extension <code>ycommercewebservices</code>. */
	BILLING_ADDRESS  

}
