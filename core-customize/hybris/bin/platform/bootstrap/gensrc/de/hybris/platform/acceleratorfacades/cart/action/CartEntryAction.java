/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:25
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorfacades.cart.action;

/**
 * Accelerator cart entry actions
 */
public enum CartEntryAction
{

	/** <i>Generated enum value</i> for <code>CartEntryAction.REMOVE</code> value defined at extension <code>acceleratorfacades</code>. */
	REMOVE  

}
