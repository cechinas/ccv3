/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:30
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.company.data;

/**
 * @deprecated Since 6.0. Use {@link de.hybris.platform.commercefacades.user.data.CustomerData} instead.
 */
@Deprecated(forRemoval = true)
public  class UserData extends de.hybris.platform.b2bcommercefacades.company.data.UserData 
{

 
	
	public UserData()
	{
		// default constructor
	}
	


}
