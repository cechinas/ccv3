/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:24
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.warehousing.data.sourcing;

public  class FitSourcingLocation extends SourcingLocation 
{

 

	/** <i>Generated property</i> for <code>FitSourcingLocation.fitness</code> property defined at extension <code>warehousing</code>. */
		
	private Double fitness;
	
	public FitSourcingLocation()
	{
		// default constructor
	}
	
	public void setFitness(final Double fitness)
	{
		this.fitness = fitness;
	}

	public Double getFitness() 
	{
		return fitness;
	}
	


}
