/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:24
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.search.data;

/**
 * @deprecated true
 */
@Deprecated(forRemoval = true)
public  class BudgetSearchStateData extends de.hybris.platform.b2bcommercefacades.search.data.BudgetSearchStateData 
{

 
	
	public BudgetSearchStateData()
	{
		// default constructor
	}
	


}
