/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:23
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cms2.enums;

public enum SortDirection
{

	/** <i>Generated enum value</i> for <code>SortDirection.ASC</code> value defined at extension <code>cms2</code>. */
	ASC , 
	/** <i>Generated enum value</i> for <code>SortDirection.DESC</code> value defined at extension <code>cms2</code>. */
	DESC  

}
