/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:30
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking;

import java.io.Serializable;
import de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking.CarrierWsDTO;
import de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking.ConsignmentTrackingEventDataWsDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;

/**
 * Consignment tracking data
 */
@ApiModel(value="consignmentTracking", description="Consignment tracking data")
public  class ConsignmentTrackingWsDTO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** consignment status<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.statusDisplay</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="statusDisplay", value="consignment status", example="inTransit,delivering,deliveryCompleted,deliveryRejected") 	
	private String statusDisplay;

	/** carrier details<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.carrierDetails</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="carrierDetails", value="carrier details") 	
	private CarrierWsDTO carrierDetails;

	/** tracking id<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.trackingID</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingID", value="tracking id") 	
	private String trackingID;

	/** the tracking url provided by the carrier<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.trackingUrl</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingUrl", value="the tracking url provided by the carrier") 	
	private String trackingUrl;

	/** target arrival date<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.targetArrivalDate</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="targetArrivalDate", value="target arrival date") 	
	private Date targetArrivalDate;

	/** logistics tracking information<br/><br/><i>Generated property</i> for <code>ConsignmentTrackingWsDTO.trackingEvents</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingEvents", value="logistics tracking information") 	
	private List<ConsignmentTrackingEventDataWsDTO> trackingEvents;
	
	public ConsignmentTrackingWsDTO()
	{
		// default constructor
	}
	
	public void setStatusDisplay(final String statusDisplay)
	{
		this.statusDisplay = statusDisplay;
	}

	public String getStatusDisplay() 
	{
		return statusDisplay;
	}
	
	public void setCarrierDetails(final CarrierWsDTO carrierDetails)
	{
		this.carrierDetails = carrierDetails;
	}

	public CarrierWsDTO getCarrierDetails() 
	{
		return carrierDetails;
	}
	
	public void setTrackingID(final String trackingID)
	{
		this.trackingID = trackingID;
	}

	public String getTrackingID() 
	{
		return trackingID;
	}
	
	public void setTrackingUrl(final String trackingUrl)
	{
		this.trackingUrl = trackingUrl;
	}

	public String getTrackingUrl() 
	{
		return trackingUrl;
	}
	
	public void setTargetArrivalDate(final Date targetArrivalDate)
	{
		this.targetArrivalDate = targetArrivalDate;
	}

	public Date getTargetArrivalDate() 
	{
		return targetArrivalDate;
	}
	
	public void setTrackingEvents(final List<ConsignmentTrackingEventDataWsDTO> trackingEvents)
	{
		this.trackingEvents = trackingEvents;
	}

	public List<ConsignmentTrackingEventDataWsDTO> getTrackingEvents() 
	{
		return trackingEvents;
	}
	


}
