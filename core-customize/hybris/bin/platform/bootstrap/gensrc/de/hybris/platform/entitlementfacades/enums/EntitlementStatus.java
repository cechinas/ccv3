/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:27
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.entitlementfacades.enums;

public enum EntitlementStatus
{

	/** <i>Generated enum value</i> for <code>EntitlementStatus.ACTIVE</code> value defined at extension <code>entitlementfacades</code>. */
	ACTIVE , 
	/** <i>Generated enum value</i> for <code>EntitlementStatus.SUSPENDED</code> value defined at extension <code>entitlementfacades</code>. */
	SUSPENDED , 
	/** <i>Generated enum value</i> for <code>EntitlementStatus.REVOKED</code> value defined at extension <code>entitlementfacades</code>. */
	REVOKED  

}
