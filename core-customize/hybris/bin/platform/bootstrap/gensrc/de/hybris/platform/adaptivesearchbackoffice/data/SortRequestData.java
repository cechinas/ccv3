/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:30
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.adaptivesearchbackoffice.data;

import de.hybris.platform.adaptivesearchbackoffice.data.AbstractSearchRequestData;

public  class SortRequestData extends AbstractSearchRequestData 
{

 

	/** <i>Generated property</i> for <code>SortRequestData.sort</code> property defined at extension <code>adaptivesearchbackoffice</code>. */
		
	private String sort;
	
	public SortRequestData()
	{
		// default constructor
	}
	
	public void setSort(final String sort)
	{
		this.sort = sort;
	}

	public String getSort() 
	{
		return sort;
	}
	


}
