/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:29
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

public enum RuleIrGroupOperator
{

	/** <i>Generated enum value</i> for <code>RuleIrGroupOperator.AND</code> value defined at extension <code>ruleengineservices</code>. */
	AND , 
	/** <i>Generated enum value</i> for <code>RuleIrGroupOperator.OR</code> value defined at extension <code>ruleengineservices</code>. */
	OR  

}
