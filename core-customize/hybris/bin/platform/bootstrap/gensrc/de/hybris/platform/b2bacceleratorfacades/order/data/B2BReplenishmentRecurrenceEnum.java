/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:31
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.order.data;

public enum B2BReplenishmentRecurrenceEnum
{

	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.DAILY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	DAILY , 
	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.WEEKLY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	WEEKLY , 
	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.MONTHLY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	MONTHLY  

}
