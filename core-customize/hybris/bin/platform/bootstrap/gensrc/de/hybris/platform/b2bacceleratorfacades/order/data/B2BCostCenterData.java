/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:27
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.order.data;

/**
 * @deprecated Since 6.0. Use {@link de.hybris.platform.b2bcommercefacades.company.data.B2BCostCenterData} instead.
 */
@Deprecated(forRemoval = true)
public  class B2BCostCenterData extends de.hybris.platform.b2bcommercefacades.company.data.B2BCostCenterData 
{

 
	
	public B2BCostCenterData()
	{
		// default constructor
	}
	


}
