/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 10 feb 2020 15:41:25
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.customerticketingfacades.data;

public enum TicketCategory
{

	/** <i>Generated enum value</i> for <code>TicketCategory.Enquiry</code> value defined at extension <code>customerticketingfacades</code>. */
	ENQUIRY , 
	/** <i>Generated enum value</i> for <code>TicketCategory.Complaint</code> value defined at extension <code>customerticketingfacades</code>. */
	COMPLAINT , 
	/** <i>Generated enum value</i> for <code>TicketCategory.Problem</code> value defined at extension <code>customerticketingfacades</code>. */
	PROBLEM  

}
