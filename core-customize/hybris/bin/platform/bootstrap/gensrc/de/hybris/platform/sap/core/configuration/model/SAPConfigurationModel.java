/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.core.configuration.model;

import de.hybris.bootstrap.annotations.Accessor;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.sap.core.configuration.model.SAPRFCDestinationModel;
import de.hybris.platform.sap.sapmodel.model.SAPDeliveryModeModel;
import de.hybris.platform.sap.sapmodel.model.SAPPaymentModeModel;
import de.hybris.platform.sap.sapmodel.model.SAPPlantLogSysOrgModel;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.store.BaseStoreModel;
import java.util.Collection;
import java.util.Set;

/**
 * Generated model class for type SAPConfiguration first defined at extension sapcoreconfiguration.
 */
@SuppressWarnings("all")
public class SAPConfigurationModel extends ItemModel
{
	/**<i>Generated model type code constant.</i>*/
	public static final String _TYPECODE = "SAPConfiguration";
	
	/**<i>Generated relation code constant for relation <code>SAPConfigurationForBaseStore</code> defining source attribute <code>baseStores</code> in extension <code>sapcoreconfiguration</code>.</i>*/
	public static final String _SAPCONFIGURATIONFORBASESTORE = "SAPConfigurationForBaseStore";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.core_name</code> attribute defined at extension <code>sapcoreconfiguration</code>. */
	public static final String CORE_NAME = "core_name";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.baseStores</code> attribute defined at extension <code>sapcoreconfiguration</code>. */
	public static final String BASESTORES = "baseStores";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.SAPRFCDestination</code> attribute defined at extension <code>sapcoreconfiguration</code>. */
	public static final String SAPRFCDESTINATION = "SAPRFCDestination";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapcommon_referenceCustomer</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPCOMMON_REFERENCECUSTOMER = "sapcommon_referenceCustomer";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapcommon_transactionType</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPCOMMON_TRANSACTIONTYPE = "sapcommon_transactionType";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapcommon_salesOrganization</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPCOMMON_SALESORGANIZATION = "sapcommon_salesOrganization";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapcommon_distributionChannel</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPCOMMON_DISTRIBUTIONCHANNEL = "sapcommon_distributionChannel";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapcommon_division</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPCOMMON_DIVISION = "sapcommon_division";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapDeliveryModes</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPDELIVERYMODES = "sapDeliveryModes";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapPaymentModes</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPPAYMENTMODES = "sapPaymentModes";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.sapPlantLogSysOrg</code> attribute defined at extension <code>sapmodel</code>. */
	public static final String SAPPLANTLOGSYSORG = "sapPlantLogSysOrg";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.saporderexchange_itemPriceConditionType</code> attribute defined at extension <code>saporderexchange</code>. */
	public static final String SAPORDEREXCHANGE_ITEMPRICECONDITIONTYPE = "saporderexchange_itemPriceConditionType";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.saporderexchange_paymentCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. */
	public static final String SAPORDEREXCHANGE_PAYMENTCOSTCONDITIONTYPE = "saporderexchange_paymentCostConditionType";
	
	/** <i>Generated constant</i> - Attribute key of <code>SAPConfiguration.saporderexchange_deliveryCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. */
	public static final String SAPORDEREXCHANGE_DELIVERYCOSTCONDITIONTYPE = "saporderexchange_deliveryCostConditionType";
	
	
	/**
	 * <i>Generated constructor</i> - Default constructor for generic creation.
	 */
	public SAPConfigurationModel()
	{
		super();
	}
	
	/**
	 * <i>Generated constructor</i> - Default constructor for creation with existing context
	 * @param ctx the model context to be injected, must not be null
	 */
	public SAPConfigurationModel(final ItemModelContext ctx)
	{
		super(ctx);
	}
	
	/**
	 * <i>Generated constructor</i> - for all mandatory and initial attributes.
	 * @deprecated since 4.1.1 Please use the default constructor without parameters
	 * @param _owner initial attribute declared by type <code>Item</code> at extension <code>core</code>
	 */
	@Deprecated(since = "4.1.1", forRemoval = true)
	public SAPConfigurationModel(final ItemModel _owner)
	{
		super();
		setOwner(_owner);
	}
	
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.baseStores</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 * Consider using FlexibleSearchService::searchRelation for pagination support of large result sets.
	 * @return the baseStores
	 */
	@Accessor(qualifier = "baseStores", type = Accessor.Type.GETTER)
	public Collection<BaseStoreModel> getBaseStores()
	{
		return getPersistenceContext().getPropertyValue(BASESTORES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.core_name</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 * @return the core_name - Name
	 */
	@Accessor(qualifier = "core_name", type = Accessor.Type.GETTER)
	public String getCore_name()
	{
		return getPersistenceContext().getPropertyValue(CORE_NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapcommon_distributionChannel</code> attribute defined at extension <code>sapmodel</code>. 
	 * @return the sapcommon_distributionChannel
	 */
	@Accessor(qualifier = "sapcommon_distributionChannel", type = Accessor.Type.GETTER)
	public String getSapcommon_distributionChannel()
	{
		return getPersistenceContext().getPropertyValue(SAPCOMMON_DISTRIBUTIONCHANNEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapcommon_division</code> attribute defined at extension <code>sapmodel</code>. 
	 * @return the sapcommon_division
	 */
	@Accessor(qualifier = "sapcommon_division", type = Accessor.Type.GETTER)
	public String getSapcommon_division()
	{
		return getPersistenceContext().getPropertyValue(SAPCOMMON_DIVISION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapcommon_referenceCustomer</code> attribute defined at extension <code>sapmodel</code>. 
	 * @return the sapcommon_referenceCustomer - reference customer used in RFC call
	 */
	@Accessor(qualifier = "sapcommon_referenceCustomer", type = Accessor.Type.GETTER)
	public String getSapcommon_referenceCustomer()
	{
		return getPersistenceContext().getPropertyValue(SAPCOMMON_REFERENCECUSTOMER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapcommon_salesOrganization</code> attribute defined at extension <code>sapmodel</code>. 
	 * @return the sapcommon_salesOrganization
	 */
	@Accessor(qualifier = "sapcommon_salesOrganization", type = Accessor.Type.GETTER)
	public String getSapcommon_salesOrganization()
	{
		return getPersistenceContext().getPropertyValue(SAPCOMMON_SALESORGANIZATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapcommon_transactionType</code> attribute defined at extension <code>sapmodel</code>. 
	 * @return the sapcommon_transactionType
	 */
	@Accessor(qualifier = "sapcommon_transactionType", type = Accessor.Type.GETTER)
	public String getSapcommon_transactionType()
	{
		return getPersistenceContext().getPropertyValue(SAPCOMMON_TRANSACTIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapDeliveryModes</code> attribute defined at extension <code>sapmodel</code>. 
	 * Consider using FlexibleSearchService::searchRelation for pagination support of large result sets.
	 * @return the sapDeliveryModes
	 */
	@Accessor(qualifier = "sapDeliveryModes", type = Accessor.Type.GETTER)
	public Set<SAPDeliveryModeModel> getSapDeliveryModes()
	{
		return getPersistenceContext().getPropertyValue(SAPDELIVERYMODES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.saporderexchange_deliveryCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 * @return the saporderexchange_deliveryCostConditionType - Condition Type for delivery cost.
	 */
	@Accessor(qualifier = "saporderexchange_deliveryCostConditionType", type = Accessor.Type.GETTER)
	public String getSaporderexchange_deliveryCostConditionType()
	{
		return getPersistenceContext().getPropertyValue(SAPORDEREXCHANGE_DELIVERYCOSTCONDITIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.saporderexchange_itemPriceConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 * @return the saporderexchange_itemPriceConditionType - Condition Type for item Price.
	 */
	@Accessor(qualifier = "saporderexchange_itemPriceConditionType", type = Accessor.Type.GETTER)
	public String getSaporderexchange_itemPriceConditionType()
	{
		return getPersistenceContext().getPropertyValue(SAPORDEREXCHANGE_ITEMPRICECONDITIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.saporderexchange_paymentCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 * @return the saporderexchange_paymentCostConditionType - Condition Type for payment cost.
	 */
	@Accessor(qualifier = "saporderexchange_paymentCostConditionType", type = Accessor.Type.GETTER)
	public String getSaporderexchange_paymentCostConditionType()
	{
		return getPersistenceContext().getPropertyValue(SAPORDEREXCHANGE_PAYMENTCOSTCONDITIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapPaymentModes</code> attribute defined at extension <code>sapmodel</code>. 
	 * Consider using FlexibleSearchService::searchRelation for pagination support of large result sets.
	 * @return the sapPaymentModes
	 */
	@Accessor(qualifier = "sapPaymentModes", type = Accessor.Type.GETTER)
	public Set<SAPPaymentModeModel> getSapPaymentModes()
	{
		return getPersistenceContext().getPropertyValue(SAPPAYMENTMODES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.sapPlantLogSysOrg</code> attribute defined at extension <code>sapmodel</code>. 
	 * Consider using FlexibleSearchService::searchRelation for pagination support of large result sets.
	 * @return the sapPlantLogSysOrg
	 */
	@Accessor(qualifier = "sapPlantLogSysOrg", type = Accessor.Type.GETTER)
	public Set<SAPPlantLogSysOrgModel> getSapPlantLogSysOrg()
	{
		return getPersistenceContext().getPropertyValue(SAPPLANTLOGSYSORG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPConfiguration.SAPRFCDestination</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 * @return the SAPRFCDestination
	 */
	@Accessor(qualifier = "SAPRFCDestination", type = Accessor.Type.GETTER)
	public SAPRFCDestinationModel getSAPRFCDestination()
	{
		return getPersistenceContext().getPropertyValue(SAPRFCDESTINATION);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.baseStores</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 *  
	 * @param value the baseStores
	 */
	@Accessor(qualifier = "baseStores", type = Accessor.Type.SETTER)
	public void setBaseStores(final Collection<BaseStoreModel> value)
	{
		getPersistenceContext().setPropertyValue(BASESTORES, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.core_name</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 *  
	 * @param value the core_name - Name
	 */
	@Accessor(qualifier = "core_name", type = Accessor.Type.SETTER)
	public void setCore_name(final String value)
	{
		getPersistenceContext().setPropertyValue(CORE_NAME, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapcommon_distributionChannel</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapcommon_distributionChannel
	 */
	@Accessor(qualifier = "sapcommon_distributionChannel", type = Accessor.Type.SETTER)
	public void setSapcommon_distributionChannel(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPCOMMON_DISTRIBUTIONCHANNEL, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapcommon_division</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapcommon_division
	 */
	@Accessor(qualifier = "sapcommon_division", type = Accessor.Type.SETTER)
	public void setSapcommon_division(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPCOMMON_DIVISION, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapcommon_referenceCustomer</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapcommon_referenceCustomer - reference customer used in RFC call
	 */
	@Accessor(qualifier = "sapcommon_referenceCustomer", type = Accessor.Type.SETTER)
	public void setSapcommon_referenceCustomer(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPCOMMON_REFERENCECUSTOMER, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapcommon_salesOrganization</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapcommon_salesOrganization
	 */
	@Accessor(qualifier = "sapcommon_salesOrganization", type = Accessor.Type.SETTER)
	public void setSapcommon_salesOrganization(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPCOMMON_SALESORGANIZATION, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapcommon_transactionType</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapcommon_transactionType
	 */
	@Accessor(qualifier = "sapcommon_transactionType", type = Accessor.Type.SETTER)
	public void setSapcommon_transactionType(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPCOMMON_TRANSACTIONTYPE, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapDeliveryModes</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapDeliveryModes
	 */
	@Accessor(qualifier = "sapDeliveryModes", type = Accessor.Type.SETTER)
	public void setSapDeliveryModes(final Set<SAPDeliveryModeModel> value)
	{
		getPersistenceContext().setPropertyValue(SAPDELIVERYMODES, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.saporderexchange_deliveryCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 *  
	 * @param value the saporderexchange_deliveryCostConditionType - Condition Type for delivery cost.
	 */
	@Accessor(qualifier = "saporderexchange_deliveryCostConditionType", type = Accessor.Type.SETTER)
	public void setSaporderexchange_deliveryCostConditionType(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPORDEREXCHANGE_DELIVERYCOSTCONDITIONTYPE, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.saporderexchange_itemPriceConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 *  
	 * @param value the saporderexchange_itemPriceConditionType - Condition Type for item Price.
	 */
	@Accessor(qualifier = "saporderexchange_itemPriceConditionType", type = Accessor.Type.SETTER)
	public void setSaporderexchange_itemPriceConditionType(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPORDEREXCHANGE_ITEMPRICECONDITIONTYPE, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.saporderexchange_paymentCostConditionType</code> attribute defined at extension <code>saporderexchange</code>. 
	 *  
	 * @param value the saporderexchange_paymentCostConditionType - Condition Type for payment cost.
	 */
	@Accessor(qualifier = "saporderexchange_paymentCostConditionType", type = Accessor.Type.SETTER)
	public void setSaporderexchange_paymentCostConditionType(final String value)
	{
		getPersistenceContext().setPropertyValue(SAPORDEREXCHANGE_PAYMENTCOSTCONDITIONTYPE, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapPaymentModes</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapPaymentModes
	 */
	@Accessor(qualifier = "sapPaymentModes", type = Accessor.Type.SETTER)
	public void setSapPaymentModes(final Set<SAPPaymentModeModel> value)
	{
		getPersistenceContext().setPropertyValue(SAPPAYMENTMODES, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.sapPlantLogSysOrg</code> attribute defined at extension <code>sapmodel</code>. 
	 *  
	 * @param value the sapPlantLogSysOrg
	 */
	@Accessor(qualifier = "sapPlantLogSysOrg", type = Accessor.Type.SETTER)
	public void setSapPlantLogSysOrg(final Set<SAPPlantLogSysOrgModel> value)
	{
		getPersistenceContext().setPropertyValue(SAPPLANTLOGSYSORG, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>SAPConfiguration.SAPRFCDestination</code> attribute defined at extension <code>sapcoreconfiguration</code>. 
	 *  
	 * @param value the SAPRFCDestination
	 */
	@Accessor(qualifier = "SAPRFCDestination", type = Accessor.Type.SETTER)
	public void setSAPRFCDestination(final SAPRFCDestinationModel value)
	{
		getPersistenceContext().setPropertyValue(SAPRFCDESTINATION, value);
	}
	
}
