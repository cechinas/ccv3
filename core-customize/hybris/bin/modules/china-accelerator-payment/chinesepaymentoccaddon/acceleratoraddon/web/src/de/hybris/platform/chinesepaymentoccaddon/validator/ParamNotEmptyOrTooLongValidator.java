/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.chinesepaymentoccaddon.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/*
 * Validates if the field is empty or too long.
 */
public class ParamNotEmptyOrTooLongValidator implements Validator
{
	private final String fieldName;
	private final int maxLength;

	public ParamNotEmptyOrTooLongValidator(final String fieldName, final int maxLength)
	{
		this.fieldName = fieldName;
		this.maxLength = maxLength;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return String.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final String fieldValue = (String) object;
		if (StringUtils.isBlank(fieldValue) || StringUtils.length(fieldValue) > getMaxLength())
		{
			errors.reject("chinesepayment.param.notEmptyOrTooLong", new String[]
			{ fieldName, String.valueOf(getMaxLength()) },
					"The {0} is required and must be between 1 and {1} characters long.");
		}
	}

	protected String getFieldName()
	{
		return this.fieldName;
	}


	protected int getMaxLength()
	{
		return this.maxLength;
	}
}
