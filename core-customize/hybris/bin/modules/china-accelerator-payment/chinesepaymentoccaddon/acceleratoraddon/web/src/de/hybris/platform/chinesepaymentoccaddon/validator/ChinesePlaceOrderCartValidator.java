/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.chinesepaymentoccaddon.validator;

import de.hybris.platform.commercefacades.order.data.CartData;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates cart before placing order.
 */
public class ChinesePlaceOrderCartValidator implements Validator
{
	@Override
	public boolean supports(final Class<?> clazz)
	{
		return CartData.class.equals(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final CartData cart = (CartData) target;

		if (!cart.isCalculated())
		{
			errors.reject("chinesepayment.cart.notCalculated", "Cart is not calculated.");
		}

		if (cart.getDeliveryAddress() == null)
		{
			errors.reject("chinesepayment.cart.deliveryAddressNotSet", "Delivery address is not set.");
		}

		if (cart.getDeliveryMode() == null)
		{
			errors.reject("chinesepayment.cart.deliveryModeNotSet", "Delivery mode is not set.");
		}

		if (cart.getChinesePaymentInfo() == null)
		{
			errors.reject("chinesepayment.cart.chinesePaymentInfoNotSet", "Chinese payment info is not set.");
		}

	}
}
