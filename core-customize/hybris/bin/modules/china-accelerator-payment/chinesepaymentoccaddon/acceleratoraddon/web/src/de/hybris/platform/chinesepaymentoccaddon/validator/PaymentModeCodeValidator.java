/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.chinesepaymentoccaddon.validator;

import de.hybris.platform.chinesepaymentfacades.checkout.ChineseCheckoutFacade;

import java.util.Objects;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates PaymentMode code.
 */
public class PaymentModeCodeValidator implements Validator
{

	private final ChineseCheckoutFacade chineseCheckoutFacade;

	private final Validator paymentModeCodeNotEmptyOrTooLongValidator;

	public PaymentModeCodeValidator(final ChineseCheckoutFacade chineseCheckoutFacade,
			final Validator paymentModeCodeNotEmptyOrTooLongValidator)
	{
		this.chineseCheckoutFacade = chineseCheckoutFacade;
		this.paymentModeCodeNotEmptyOrTooLongValidator = paymentModeCodeNotEmptyOrTooLongValidator;
	}

	@Override
	public boolean supports(final Class clazz)
	{
		return String.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		paymentModeCodeNotEmptyOrTooLongValidator.validate(target, errors);
		final String paymentModeCode = (String) target;
		if (!errors.hasErrors() && Objects.isNull(chineseCheckoutFacade.getPaymentModeByCode(paymentModeCode)))
		{
			errors.reject("chinesepayment.param.invalid", new String[]
			{ "paymentModeCode" }, "The {0} is not valid.");
		}
	}

}
