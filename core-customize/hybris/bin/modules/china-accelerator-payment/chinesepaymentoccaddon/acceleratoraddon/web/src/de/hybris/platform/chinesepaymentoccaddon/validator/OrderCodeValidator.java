/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.chinesepaymentoccaddon.validator;

import de.hybris.platform.chinesepaymentfacades.payment.strategies.OrderPayableCheckStrategy;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Objects;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates Order code.
 */
public class OrderCodeValidator implements Validator
{
	private final OrderFacade orderFacade;

	private final OrderPayableCheckStrategy chineseOrderPayableCheckStrategy;

	private final Validator orderCodeNotEmptyOrTooLongValidator;


	public OrderCodeValidator(final OrderFacade orderFacade,
			final OrderPayableCheckStrategy chineseOrderPayableCheckStrategy, final Validator orderCodeNotEmptyOrTooLongValidator)
	{
		this.orderFacade = orderFacade;
		this.chineseOrderPayableCheckStrategy = chineseOrderPayableCheckStrategy;
		this.orderCodeNotEmptyOrTooLongValidator = orderCodeNotEmptyOrTooLongValidator;
	}

	@Override
	public boolean supports(final Class clazz)
	{
		return String.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		orderCodeNotEmptyOrTooLongValidator.validate(target, errors);
		if (!errors.hasErrors())
		{
			final String orderCode = (String) target;
			OrderData orderData = null;
			try
			{
				orderData = orderFacade.getOrderDetailsForCode(orderCode);
			}
			catch (final UnknownIdentifierException e)
			{
				errors.reject("chinesepayment.order.invalid", new String[]
				{ orderCode }, "The order does not exist or belongs to another customer.");
			}
			if (Objects.nonNull(orderData) && !chineseOrderPayableCheckStrategy.isOrderPayable(orderData))
			{
				errors.reject("chinesepayment.order.alreadyPaid", new String[]
				{ orderCode }, "The order has already been paid.");
			}
		}
	}
}
