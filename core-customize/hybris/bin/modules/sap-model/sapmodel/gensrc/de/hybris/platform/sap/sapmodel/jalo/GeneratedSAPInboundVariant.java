/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapmodel.jalo;

import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.sap.sapmodel.constants.SapmodelConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapmodel.jalo.SAPInboundVariant SAPInboundVariant}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPInboundVariant extends GenericItem
{
	/** Qualifier of the <code>SAPInboundVariant.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>SAPInboundVariant.catalogVersion</code> attribute **/
	public static final String CATALOGVERSION = "catalogVersion";
	/** Qualifier of the <code>SAPInboundVariant.baseProduct</code> attribute **/
	public static final String BASEPRODUCT = "baseProduct";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(CATALOGVERSION, AttributeMode.INITIAL);
		tmp.put(BASEPRODUCT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.baseProduct</code> attribute.
	 * @return the baseProduct - Base Product
	 */
	public Product getBaseProduct(final SessionContext ctx)
	{
		return (Product)getProperty( ctx, BASEPRODUCT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.baseProduct</code> attribute.
	 * @return the baseProduct - Base Product
	 */
	public Product getBaseProduct()
	{
		return getBaseProduct( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.baseProduct</code> attribute. 
	 * @param value the baseProduct - Base Product
	 */
	public void setBaseProduct(final SessionContext ctx, final Product value)
	{
		setProperty(ctx, BASEPRODUCT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.baseProduct</code> attribute. 
	 * @param value the baseProduct - Base Product
	 */
	public void setBaseProduct(final Product value)
	{
		setBaseProduct( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.catalogVersion</code> attribute.
	 * @return the catalogVersion - Catalog Version
	 */
	public CatalogVersion getCatalogVersion(final SessionContext ctx)
	{
		return (CatalogVersion)getProperty( ctx, CATALOGVERSION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.catalogVersion</code> attribute.
	 * @return the catalogVersion - Catalog Version
	 */
	public CatalogVersion getCatalogVersion()
	{
		return getCatalogVersion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.catalogVersion</code> attribute. 
	 * @param value the catalogVersion - Catalog Version
	 */
	public void setCatalogVersion(final SessionContext ctx, final CatalogVersion value)
	{
		setProperty(ctx, CATALOGVERSION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.catalogVersion</code> attribute. 
	 * @param value the catalogVersion - Catalog Version
	 */
	public void setCatalogVersion(final CatalogVersion value)
	{
		setCatalogVersion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.code</code> attribute.
	 * @return the code - Product Code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPInboundVariant.code</code> attribute.
	 * @return the code - Product Code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.code</code> attribute. 
	 * @param value the code - Product Code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPInboundVariant.code</code> attribute. 
	 * @param value the code - Product Code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
}
