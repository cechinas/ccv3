/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.integrationbackoffice.widgets.controllers;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.integrationbackoffice.dto.ListItemDTO;
import de.hybris.platform.integrationbackoffice.services.ReadService;
import de.hybris.platform.integrationbackoffice.widgets.editor.controllers.IntegrationObjectEditorController;
import de.hybris.platform.integrationbackoffice.widgets.editor.utility.EditorUtils;
import de.hybris.platform.integrationservices.model.IntegrationObjectItemModel;
import de.hybris.platform.integrationservices.model.IntegrationObjectModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class IntegrationObjectEditorControllerIntegrationTest extends ServicelayerTest {

    @Resource
    private FlexibleSearchService flexibleSearchService;
//    @Resource
//    private ReadService readService;

    private IntegrationObjectModel integrationObjectModel;
    private IntegrationObjectEditorController controller = new IntegrationObjectEditorController();

    @Before
    public void setUp() throws Exception {
        importCsv("/test/impex/SingleRootTestImpex001.csv", "UTF-8");
        importCsv("/test/impex/NoRootTestImpex002.csv", "UTF-8");
        importCsv("/test/impex/MultiRootTestImpex003.csv", "UTF-8");
        importCsv("/test/impex/SingleRootCircularDepTestImpex004.csv", "UTF-8");
        importCsv("/test/impex/MultiRootCircularDepTestImpex005.csv", "UTF-8");
        importCsv("/test/impex/PopulateAttributesMapTestImpex006.csv", "UTF-8");
    }

    @Test
    public void getIntegrationObjectSingleBooleanRoot() {
        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'SingleRootTestImpex001')");
        integrationObjectModel = search.getResult().get(0);

        final String expectedRoot = "OrgUnit";
        final String actualRoot = controller.getIntegrationObjectRoot(integrationObjectModel).getCode();

        assertEquals("SingleRootTestImpex001", integrationObjectModel.getCode());
        assertEquals(expectedRoot, actualRoot);
    }

    @Test
    public void getIntegrationObjectNoBooleanRoot() {
        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'NoRootTestImpex002')");
        integrationObjectModel = search.getResult().get(0);

        final String expectedRoot = "Category";
        final String actualRoot = controller.getIntegrationObjectRoot(integrationObjectModel).getCode();

        assertEquals("NoRootTestImpex002", integrationObjectModel.getCode());
        assertEquals(expectedRoot, actualRoot);
    }

    @Test
    public void getIntegrationObjectMultiBooleanRoot() {
        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'MultiRootTestImpex003')");
        integrationObjectModel = search.getResult().get(0);

        integrationObjectModel.getItems().forEach(item -> {
            if (item.getCode().equals("Product")) {
                item.setRoot(true);
            }
        });

        final String expectedRoot = "Category";
        final String actualRoot = controller.getIntegrationObjectRoot(integrationObjectModel).getCode();

        assertEquals("MultiRootTestImpex003", integrationObjectModel.getCode());
        assertEquals(expectedRoot, actualRoot);
    }

    @Test
    public void getIntegrationObjectSingleBooleanRootCircularDep() {
        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'SingleRootCircularDepTestImpex004')");
        integrationObjectModel = search.getResult().get(0);

        final String expectedRoot = "Order";
        final String actualRoot = controller.getIntegrationObjectRoot(integrationObjectModel).getCode();

        assertEquals("SingleRootCircularDepTestImpex004", integrationObjectModel.getCode());
        assertEquals(expectedRoot, actualRoot);
    }

    @Test
    public void getIntegrationObjectMultiBooleanRootCircularDep() {
        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'MultiRootCircularDepTestImpex005')");
        integrationObjectModel = search.getResult().get(0);

        integrationObjectModel.getItems().forEach(item -> {
            if (item.getCode().equals("OrderEntry")) {
                item.setRoot(true);
            }
        });

        final IntegrationObjectItemModel integrationObjectItemModel = controller.getIntegrationObjectRoot(integrationObjectModel);
        String actualRoot;

        if (integrationObjectItemModel == null) {
            actualRoot = null;
        } else {
            actualRoot = "Test fails";
        }

        assertEquals("MultiRootCircularDepTestImpex005", integrationObjectModel.getCode());
        assertNull(actualRoot);
    }

//    @Test
//    public void populateAttributesMapTest() {
//        final SearchResult<IntegrationObjectModel> search = flexibleSearchService.search("SELECT PK FROM {IntegrationObject} WHERE (p_code = 'PopulateAttributesMapTestImpex006')");
//        integrationObjectModel = search.getResult().get(0);
//        assertEquals("PopulateAttributesMapTestImpex006", integrationObjectModel.getCode());
//
//        ComposedTypeModel root = controller.getIntegrationObjectRoot(integrationObjectModel).getType();
//        controller.setCurrentAttributesMap(new HashMap<>());
//        controller.populateAttributesMap(root);
//        Map<ComposedTypeModel, List<ListItemDTO>> actualMap = controller.getCurrentAttributesMap();
//
//        //Correct loaded impex
//        Map<ComposedTypeModel, List<ListItemDTO>> expectedMap = EditorUtils.convertIntegrationObjectToDTOMap(readService, integrationObjectModel);
//        for (ComposedTypeModel key : expectedMap.keySet()) {
//            if (key.getCode().equals("Warehouse")) {
//                expectedMap.remove(key);
//                continue;
//            }
//            expectedMap.get(key).forEach(dto -> {
//                dto.setCustomUnique(false);
//                dto.setSelected(false);
//            });
//
//        }
//
//        assertTrue(areMapValuesEqual(actualMap, expectedMap));
//        //import impex set to IO
//        //grab root
//        //call method with readservice and root
//        //Get final map
//
//        //Import basic Vendor IO
//        //Convert to dto map
//        //Remove warehouse instance
//        //Set all selected=false, unique=false
//
//        //compare
//    }
//
//    private boolean areMapValuesEqual(Map<ComposedTypeModel, List<ListItemDTO>> first, Map<ComposedTypeModel, List<ListItemDTO>> second) {
//        if (first.size() != second.size()) {
//            return false;
//        }
//
//        return first.entrySet().stream()
//                .allMatch(e -> e.getValue().equals(second.get(e)));
//    }
}
