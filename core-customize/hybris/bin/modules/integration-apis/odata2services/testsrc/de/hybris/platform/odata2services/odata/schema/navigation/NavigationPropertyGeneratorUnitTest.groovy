/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package de.hybris.platform.odata2services.odata.schema.navigation

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.integrationservices.model.IntegrationObjectItemAttributeModel
import de.hybris.platform.integrationservices.model.MockMapAttributeDescriptorModelBuilder
import de.hybris.platform.odata2services.odata.InvalidNavigationPropertyException
import de.hybris.platform.odata2services.odata.schema.SchemaElementGenerator
import de.hybris.platform.odata2services.odata.schema.association.AssociationGenerator
import de.hybris.platform.odata2services.odata.schema.association.AssociationGeneratorRegistry
import org.apache.olingo.odata2.api.edm.provider.AnnotationAttribute
import org.apache.olingo.odata2.api.edm.provider.NavigationProperty
import org.junit.Test
import spock.lang.Specification

import static de.hybris.platform.integrationservices.model.BaseMockItemAttributeModelBuilder.collectionAttributeBuilder
import static de.hybris.platform.integrationservices.model.BaseMockItemAttributeModelBuilder.mapAttributeBuilder
import static de.hybris.platform.integrationservices.model.BaseMockItemAttributeModelBuilder.oneToOneRelationAttributeBuilder

@UnitTest
class NavigationPropertyGeneratorUnitTest extends Specification {
	private static final String PRODUCT = "Product"
	private static final String UNIT = "Unit"
	private static final String KEYWORD = "Keyword"
	private static final String CATEGORY = "Category"
	private static final String PRODUCT_UNIT = "Product_Unit"
	private static final String PRODUCT_CATEGORY = "Product_Category"
	private static final String PRODUCT_KEYWORD = "Product_Keyword"
	private static final String IS_UNIQUE = "s:IsUnique"


	def associationGeneratorRegistry = Stub(AssociationGeneratorRegistry)
	def associationGenerator = Stub(AssociationGenerator)
	def attributeListGenerator = Stub(SchemaElementGenerator)

	def generator = new NavigationPropertyGenerator()

	def setup()
	{
		generator.setAttributeListGenerator(attributeListGenerator)
		generator.setAssociationGeneratorRegistry(associationGeneratorRegistry)

	}

	@Test
	def "generate for association"()
	{
		given:
		final String unit = "unit"
		final IntegrationObjectItemAttributeModel mockAttributeDefinitionModel =
				oneToOneRelationAttributeBuilder()
						.withSource(PRODUCT)
						.withTarget(UNIT)
						.withName(UNIT)
						.build()

		associationGeneratorRegistry.getAssociationGenerator(_) >> Optional.of(associationGenerator)
		associationGenerator.getAssociationName(_) >> PRODUCT_UNIT
		associationGenerator.getSourceRole(_) >> PRODUCT
		associationGenerator.getTargetRole(_) >> UNIT
		attributeListGenerator.generate(_) >> Collections.singletonList(new AnnotationAttribute())

		when:
		final Optional<NavigationProperty> navigationPropertyOptional = generator.generate(mockAttributeDefinitionModel)

		then:
		NavigationProperty navigationProperty = navigationPropertyOptional.get()

		navigationProperty.with {
			name == unit
			fromRole == PRODUCT
			toRole == UNIT
			relationship.name == "{$PRODUCT}_{$unit}"
		}
	}

	@Test
	def "generate for collection"()
	{
		given:
		final String supercategories = "supercategories"
		final IntegrationObjectItemAttributeModel mockAttributeDefinitionModel =
				collectionAttributeBuilder()
						.withName(supercategories)
						.withSource(PRODUCT)
						.withTarget(CATEGORY)
						.build()

		associationGeneratorRegistry.getAssociationGenerator(_) >> Optional.of(associationGenerator)
		associationGenerator.getAssociationName(_) >> PRODUCT_CATEGORY
		associationGenerator.getSourceRole(_) >> PRODUCT
		associationGenerator.getTargetRole(_) >> CATEGORY
		attributeListGenerator.generate(_) >>  Collections.singletonList(new AnnotationAttribute().setName(IS_UNIQUE).setText("false"))


		when:
		final Optional navigationPropertyOptional = generator.generate(mockAttributeDefinitionModel)

		then:
		NavigationProperty navigationProperty = navigationPropertyOptional.get()

		navigationProperty.with {
			name == supercategories
			fromRole == PRODUCT
			toRole == CATEGORY
			relationship.name == "{$PRODUCT}_{$supercategories}"
			annotationAttributes.size() == 1
			annotationAttributes.get(0).with {
				name == IS_UNIQUE
				text == "false"
			}
		}
	}

	@Test
	def "generate for unique collection"()
	{
		given:
		final IntegrationObjectItemAttributeModel mockAttributeDefinitionModel =
		collectionAttributeBuilder()
				.withName("supercategories")
				.withSource(PRODUCT)
				.withTarget(CATEGORY)
				.build()

		associationGeneratorRegistry.getAssociationGenerator(_) >> Optional.of(associationGenerator)
		associationGenerator.getAssociationName(_) >> PRODUCT_CATEGORY
		associationGenerator.getSourceRole(_) >> PRODUCT
		associationGenerator.getTargetRole(_) >> CATEGORY
		attributeListGenerator.generate(_) >> Collections.singletonList(new AnnotationAttribute().setName(IS_UNIQUE).setText("true"))

		when:
		generator.generate(mockAttributeDefinitionModel)

		then:
		InvalidNavigationPropertyException e = thrown(InvalidNavigationPropertyException)
		e.message.contains("Product")
		e.message.contains("supercategories")

	}

	@Test
	def "generate for map"()
	{
		given:
		final String keywords = "keywords"
		final IntegrationObjectItemAttributeModel mockAttributeDefinitionModel =
				mapAttributeBuilder()
						.withName(keywords)
						.withSource(PRODUCT)
						.withTarget(KEYWORD)
						.build()

		associationGeneratorRegistry.getAssociationGenerator(_) >> Optional.of(associationGenerator)
		associationGenerator.getAssociationName(_) >> PRODUCT_KEYWORD
		associationGenerator.getSourceRole(_) >> PRODUCT
		associationGenerator.getTargetRole(_) >> KEYWORD

		attributeListGenerator.generate(_) >>  Collections.singletonList(new AnnotationAttribute().setName(IS_UNIQUE).setText("false"))

		when:
		final Optional navigationPropertyOptional = generator.generate(mockAttributeDefinitionModel)


		then:
		NavigationProperty navigationProperty = navigationPropertyOptional.get()

		navigationProperty.with {
			name == keywords
			fromRole == PRODUCT
			toRole == KEYWORD
			relationship.name == "{$PRODUCT}_{$keywords}"
			annotationAttributes.size() == 1
			annotationAttributes.get(0).with {
				name == IS_UNIQUE
				text == "false"
			}
		}
	}

	@Test
	def "generate for unique map"()
	{
		given:
		final String name = "keywords"
		final IntegrationObjectItemAttributeModel mockAttributeDefinitionModel =
				mapAttributeBuilder()
					.withName(name)
					.withSource(PRODUCT)
					.withTarget(KEYWORD)
					.build()

		associationGeneratorRegistry.getAssociationGenerator(_) >> Optional.of(associationGenerator)
		associationGenerator.getAssociationName(_) >> PRODUCT_KEYWORD
		associationGenerator.getSourceRole(_) >> PRODUCT
		associationGenerator.getTargetRole(_) >> KEYWORD
		attributeListGenerator.generate(_) >> Collections.singletonList(new AnnotationAttribute().setName(IS_UNIQUE).setText("true"))

		when:
		generator.generate(mockAttributeDefinitionModel)

		then:
		InvalidNavigationPropertyException e = thrown(InvalidNavigationPropertyException)
		e.message.contains("Product")
		e.message.contains("keywords")
	}

	@Test
	def "generate for simple property"()
	{
		given:
		associationGeneratorRegistry.getAssociationGenerator(_) >>  Optional.empty()

		when:
		final Optional optionalProperty = generator.generate(Stub(IntegrationObjectItemAttributeModel))

		then:
		optionalProperty.isEmpty()
	}

	@Test
	def "generate for null property"()
	{
		when:
		generator.generate(null)
		
		then:
		thrown(IllegalArgumentException)
	}
}
