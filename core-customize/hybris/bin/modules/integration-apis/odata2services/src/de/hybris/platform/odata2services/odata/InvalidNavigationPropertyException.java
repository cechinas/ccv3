/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.odata2services.odata;

import de.hybris.platform.odata2services.odata.persistence.InvalidDataException;

public class InvalidNavigationPropertyException extends InvalidDataException
{
	private static final String MESSAGE = "Cannot generate unique navigation property for collections [%s.%s]";
	private final String propertyName;

	public InvalidNavigationPropertyException(final String entityType, final String propertyName)
	{
		super(String.format(MESSAGE, entityType, propertyName), "invalid_property_definition", entityType);
		this.propertyName = propertyName;
	}

	public String getPropertyName()
	{
		return propertyName;
	}
}
