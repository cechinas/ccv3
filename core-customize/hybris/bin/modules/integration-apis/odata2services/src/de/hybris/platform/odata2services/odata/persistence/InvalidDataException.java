/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.odata2services.odata.persistence;

import de.hybris.platform.odata2services.odata.OData2ServicesException;

import org.apache.olingo.odata2.api.commons.HttpStatusCodes;
import org.apache.olingo.odata2.api.edm.EdmEntityType;

/**
 * Exception to throw for error scenarios produced by invalid Data.
 * Will result in HttpStatus 400
 */
public class InvalidDataException extends OData2ServicesException
{
	private static final HttpStatusCodes STATUS_CODE = HttpStatusCodes.BAD_REQUEST;
	private final String entityType;

	/**
	 * Constructor to create InvalidDataException
	 *
	 * @param message error message
	 * @param errorCode error code
	 */
	public InvalidDataException(final String message, final String errorCode, final String entityType)
	{
		super(message, STATUS_CODE, errorCode);
		this.entityType = entityType;
	}

	/**
	 * Constructor to create InvalidDataException
	 *
	 * @param message error message
	 * @param errorCode error code
	 * @param e exception to get Message from
	 * @param entityType entityTypeName for the current {@link EdmEntityType} that is under concern
	 */
	public InvalidDataException(final String message, final String errorCode, final Throwable e, final String entityType)
	{
		super(message, STATUS_CODE, errorCode, e);
		this.entityType = entityType;
	}

	public String getEntityType()
	{
		return entityType;
	}
}