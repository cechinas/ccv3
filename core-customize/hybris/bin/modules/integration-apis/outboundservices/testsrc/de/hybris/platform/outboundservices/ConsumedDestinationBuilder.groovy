/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices

import de.hybris.platform.apiregistryservices.model.AbstractCredentialModel
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel
import de.hybris.platform.apiregistryservices.model.DestinationTargetModel
import de.hybris.platform.apiregistryservices.model.EndpointModel
import de.hybris.platform.integrationservices.util.IntegrationTestUtil

import static BasicCredentialBuilder.basicCredentialBuilder
import static DestinationTargetBuilder.destinationTargetBuilder
import static EndpointBuilder.endpointBuilder
import static de.hybris.platform.integrationservices.util.IntegrationTestUtil.importImpEx

class ConsumedDestinationBuilder {
	private static final def DEFAULT_ID = 'test-destination'
	private static final def DEFAULT_URL = 'https://test.url.that.does.not.matter'

	private String id
	private String url
	private EndpointModel endpoint
	private AbstractCredentialModel credential
	private DestinationTargetModel destinationTarget


	static ConsumedDestinationBuilder consumedDestinationBuilder() {
		new ConsumedDestinationBuilder()
	}

	ConsumedDestinationBuilder withId(final String id) {
		this.id = id
		this
	}

	ConsumedDestinationBuilder withUrl(final String url) {
		this.url = url
		this
	}

	ConsumedDestinationBuilder withEndpoint(EndpointBuilder builder) {
		withEndpoint builder.build()
	}

	ConsumedDestinationBuilder withEndpoint(EndpointModel endpoint) {
		this.endpoint = endpoint
		this
	}

	ConsumedDestinationBuilder withCredential(AbstractCredentialBuilder builder) {
		withCredential builder.build()
	}

	ConsumedDestinationBuilder withCredential(AbstractCredentialModel credential) {
		this.credential = credential
		this
	}

	ConsumedDestinationBuilder withDestinationTarget(DestinationTargetBuilder builder) {
		withDestinationTarget builder.build()
	}

	ConsumedDestinationBuilder withDestinationTarget(DestinationTargetModel destinationTarget) {
		this.destinationTarget = destinationTarget
		this
	}

	ConsumedDestinationModel build() {
		consumedDestination(id, url, endpoint, credential, destinationTarget)
	}

	private static ConsumedDestinationModel consumedDestination(String id, String url, EndpointModel endpoint, AbstractCredentialModel credential, DestinationTargetModel target) {
		def idVal = deriveId(id)
		importImpEx(
				'INSERT_UPDATE ConsumedDestination; id[unique = true]; url              ; endpoint                      ; credential                        ; destinationTarget',
				"                                 ; $idVal           ; ${deriveUrl(url)}; ${deriveEndpoint(endpoint).pk}; ${deriveCredential(credential).pk}; ${deriveTarget(target).pk}")
		getConsumedDestinationById(idVal)
	}

	private static String deriveId(String id) {
		id ?: DEFAULT_ID
	}

	private static String deriveUrl(String url) {
		url ?: DEFAULT_URL
	}

	private static EndpointModel deriveEndpoint(EndpointModel model) {
		model ?: endpointBuilder().build()
	}

	private static AbstractCredentialModel deriveCredential(AbstractCredentialModel model) {
		model ?: basicCredentialBuilder().build()
	}

	private static DestinationTargetModel deriveTarget(DestinationTargetModel model) {
		model ?: destinationTargetBuilder().build()
	}

	private static ConsumedDestinationModel getConsumedDestinationById(final String id) {
		IntegrationTestUtil.findAny(ConsumedDestinationModel, { it.id == id }).orElse(null)
	}
}
