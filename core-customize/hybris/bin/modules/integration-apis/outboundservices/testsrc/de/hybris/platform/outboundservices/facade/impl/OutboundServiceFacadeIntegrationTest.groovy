/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices.facade.impl

import com.github.tomakehurst.wiremock.junit.WireMockRule
import de.hybris.bootstrap.annotations.IntegrationTest
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.outboundservices.facade.OutboundServiceFacade
import de.hybris.platform.outboundservices.model.OutboundRequestModel
import de.hybris.platform.outboundservices.util.OutboundMonitoringRule
import de.hybris.platform.servicelayer.ServicelayerTransactionalSpockSpecification
import de.hybris.platform.servicelayer.config.ConfigurationService
import org.junit.Rule
import org.junit.Test
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import rx.observers.TestSubscriber

import javax.annotation.Resource

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import static de.hybris.platform.integrationservices.constants.IntegrationservicesConstants.SAP_PASSPORT_HEADER_NAME
import static de.hybris.platform.integrationservices.util.IntegrationTestUtil.*
import static de.hybris.platform.outboundservices.ConsumedDestinationBuilder.consumedDestinationBuilder

@IntegrationTest
class OutboundServiceFacadeIntegrationTest extends ServicelayerTransactionalSpockSpecification {

	private static final String DESTINATION_ENDPOINT = '/odata2webservices/OutboundCatalogVersion/CatalogVersions'
	private static final String DESTINATION_ID = 'facadetestdest'
	private static final String CATALOG_VERSION_IO = 'OutboundCatalogVersion'
	private static final String CATALOG_VERSION_VERSION = 'facadeTestVersion'
	private static final boolean CATALOG_VERSION_ACTIVE = true
	private static final String CATALOG_ID = 'facadeTestCatalog'

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(wireMockConfig()
			.dynamicHttpsPort()
			.keystorePath("resources/devcerts/platform.jks")
			.keystorePassword('123456'))

	@Rule
	public OutboundMonitoringRule outboundMonitoringRule = OutboundMonitoringRule.enabled()

	@Resource
	private OutboundServiceFacade outboundServiceFacade

	@Resource
	private ConfigurationService configurationService

	private CatalogVersionModel catalogVersion

	private TestSubscriber<ResponseEntity<Map>> subscriber = new TestSubscriber<>()

	def setup() {
		importDestination()
		importCatalogVersionIntegrationObject()

		catalogVersion = importCatalogVersion(CATALOG_VERSION_VERSION, CATALOG_ID, CATALOG_VERSION_ACTIVE)
	}

	@Test
	def "send a catalog version out to the destination"() {
		given: 'stub destination server to return OK'
		stubFor(post(urlEqualTo(DESTINATION_ENDPOINT)).willReturn(ok()))

		when:
		outboundServiceFacade.send(catalogVersion, CATALOG_VERSION_IO, DESTINATION_ID).subscribe(subscriber)

		then: "destination server stub is called"
		verify(postRequestedFor(urlEqualTo(DESTINATION_ENDPOINT))
				.withRequestBody(matchActive(CATALOG_VERSION_ACTIVE))
				.withRequestBody(matchVersion(CATALOG_VERSION_VERSION))
				.withRequestBody(matchCatalogId(CATALOG_ID))
				.withHeader(SAP_PASSPORT_HEADER_NAME, matchPassport()))

		and: "observable contains response with OK HTTP status"
		subscriber.getOnNextEvents().get(0).getStatusCode() == HttpStatus.OK
	}

	@Test
	def "outbound request is logged when monitoring is enabled"() {
		given: 'stub destination server to return BAD REQUEST'
		stubFor(post(urlEqualTo(DESTINATION_ENDPOINT)).willReturn(badRequest()))

		and:
		def outboundRequestSample = new OutboundRequestModel()
		outboundRequestSample.setIntegrationKey("$CATALOG_VERSION_VERSION|$CATALOG_ID")
		assertModelDoesNotExist(outboundRequestSample)

		when:
		outboundServiceFacade.send(catalogVersion, CATALOG_VERSION_IO, DESTINATION_ID).subscribe(subscriber)

		then: "destination server stub is called"
		verify(postRequestedFor(urlEqualTo(DESTINATION_ENDPOINT)))

		and:
		assertModelExists(outboundRequestSample)
	}

	def importDestination() {
		consumedDestinationBuilder()
				.withId(DESTINATION_ID)
				.withUrl("https://localhost:${wireMockRule.httpsPort()}/$DESTINATION_ENDPOINT")
				.build()
	}

	def importCatalogVersionIntegrationObject() {
		importImpEx(
				'INSERT_UPDATE IntegrationObject; code[unique = true]   ; integrationType(code)',
				"                               ; $CATALOG_VERSION_IO   ; INBOUND",
				'INSERT_UPDATE IntegrationObjectItem; integrationObject(code)[unique = true]; code[unique = true]; type(code)     ; root[default = false]',
				"                                   ; $CATALOG_VERSION_IO                   ; Catalog            ; Catalog        ;",
				"                                   ; $CATALOG_VERSION_IO                   ; CatalogVersion     ; CatalogVersion ; true",
				'INSERT_UPDATE IntegrationObjectItemAttribute; integrationObjectItem(integrationObject(code), code)[unique = true]; attributeName[unique = true]; attributeDescriptor(enclosingType(code), qualifier); returnIntegrationObjectItem(integrationObject(code), code); unique[default = false]',
				"                                            ; $CATALOG_VERSION_IO:Catalog                                        ; id                          ; Catalog:id",
				"                                            ; $CATALOG_VERSION_IO:CatalogVersion                                 ; catalog                     ; CatalogVersion:catalog                             ; $CATALOG_VERSION_IO:Catalog",
				"                                            ; $CATALOG_VERSION_IO:CatalogVersion                                 ; version                     ; CatalogVersion:version                             ;",
				"                                            ; $CATALOG_VERSION_IO:CatalogVersion                                 ; active                      ; CatalogVersion:active                              ;")
	}

	def matchActive(boolean active) {
		matchingJsonPath("\$.[?(@.active == $active)]")
	}

	def matchVersion(String version) {
		matchingJsonPath("\$.[?(@.version == '$version')]")
	}

	def matchCatalogId(String catalogId) {
		matchingJsonPath("\$.catalog[?(@.id == '$catalogId')]")
	}

	def matchPassport() {
		matching('[\\w]+')
	}
}
