/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices.decorator.impl

import com.google.common.collect.Maps
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel
import de.hybris.platform.outboundservices.client.IntegrationRestTemplateFactory
import de.hybris.platform.outboundservices.decorator.CSRFTokenFetchingException
import de.hybris.platform.outboundservices.decorator.DecoratorContext
import de.hybris.platform.outboundservices.decorator.DecoratorExecution
import org.junit.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestOperations
import spock.lang.Specification

@UnitTest
class DefaultCsrfOutboundRequestDecoratorUnitTest extends Specification {

	public static final String TEST_URL = "http://test.url"
	public static final String TEST_TOKEN = "asdf-1234"
	public static final List TEST_COOKIES = Arrays.asList("cookieForTheTest=thisCookie")

	def decorator = new DefaultCsrfOutboundRequestDecorator()

	def integrationRestTemplateFactory = Stub(IntegrationRestTemplateFactory)
	def restOperations = Mock(RestOperations)
	def decoratorExecution = Mock(DecoratorExecution)

	def setup() {
		decorator.setIntegrationRestTemplateFactory(integrationRestTemplateFactory)

		integrationRestTemplateFactory.create(_) >> restOperations
	}

	@Test
	def "csrf token is set in the headers when additionalProperties contains valid token url Key"() {
		given:
		def decoratorContext = Stub(DecoratorContext) {
			getDestinationModel() >> Stub(ConsumedDestinationModel) {
				getAdditionalProperties() >> [csrfURL: TEST_URL]
			}
		}
		when:
		decorator.decorate(new HttpHeaders(), Maps.newHashMap(), decoratorContext, decoratorExecution)

		then:
		1 * restOperations.exchange(TEST_URL, HttpMethod.GET, _, Map.class) >> responseWithCsrfTokenHeader(TEST_TOKEN, TEST_COOKIES)

		1 * decoratorExecution.createHttpEntity(_, _, decoratorContext) >> { args ->
			assert args[0].get("X-CSRF-Token").get(0) == TEST_TOKEN
			assert args[0].get("Cookie") == TEST_COOKIES
		}
	}

	@Test
	def "csrf token is empty in the headers when additionalProperties does not contain token url Key"() {
		given:
		def decoratorContext = Stub(DecoratorContext) {
			getDestinationModel() >> Stub(ConsumedDestinationModel) {
				getAdditionalProperties() >> Collections.emptyMap()
			}
		}
		when:
		decorator.decorate(new HttpHeaders(), Maps.newHashMap(), decoratorContext, decoratorExecution)

		then:
		0 * restOperations.exchange(TEST_URL, HttpMethod.GET, _, Map.class)

		1 * decoratorExecution.createHttpEntity(_, _, decoratorContext) >> { args ->
			assert args[0].get("X-CSRF-Token") == null
		}
	}

	@Test
	def "exception is thrown when csrf token is empty"() {
		given:
		def decoratorContext = Stub(DecoratorContext) {
			getDestinationModel() >> Stub(ConsumedDestinationModel) {
				getAdditionalProperties() >> [csrfURL: TEST_URL]
			}
		}
		when:
		decorator.decorate(new HttpHeaders(), Maps.newHashMap(), decoratorContext, decoratorExecution)

		then:
		1 * restOperations.exchange(TEST_URL, HttpMethod.GET, _, Map.class) >> responseWithCsrfTokenHeader("", TEST_COOKIES)

		thrown(CSRFTokenFetchingException)
	}

	@Test
	def "exception is thrown when cookies are empty"() {
		given:
		def decoratorContext = Stub(DecoratorContext) {
			getDestinationModel() >> Stub(ConsumedDestinationModel) {
				getAdditionalProperties() >> [csrfURL: TEST_URL]
			}
		}
		when:
		decorator.decorate(new HttpHeaders(), Maps.newHashMap(), decoratorContext, decoratorExecution)

		then:
		1 * restOperations.exchange(TEST_URL, HttpMethod.GET, _, Map.class) >> responseWithCsrfTokenHeader(TEST_TOKEN, Collections.emptyList())

		thrown(CSRFTokenFetchingException)
	}

	def responseWithCsrfTokenHeader(token, cookies) {
		Stub(ResponseEntity) {
			getHeaders() >> Stub(HttpHeaders) {
				getFirst("X-CSRF-Token") >> token
				get("Set-Cookie") >> cookies
			}
		}
	}
}
