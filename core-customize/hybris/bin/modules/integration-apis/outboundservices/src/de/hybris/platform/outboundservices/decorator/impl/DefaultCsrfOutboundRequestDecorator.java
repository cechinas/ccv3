/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices.decorator.impl;

import de.hybris.platform.apiregistryservices.model.ConsumedDestinationModel;
import de.hybris.platform.outboundservices.client.IntegrationRestTemplateFactory;
import de.hybris.platform.outboundservices.decorator.CSRFTokenFetchingException;
import de.hybris.platform.outboundservices.decorator.DecoratorContext;
import de.hybris.platform.outboundservices.decorator.DecoratorExecution;
import de.hybris.platform.outboundservices.decorator.OutboundRequestDecorator;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestOperations;

import com.google.common.base.Strings;

/**
 * Decorates SAP CPI outbound requests with CSRF tokens.
 */
public class DefaultCsrfOutboundRequestDecorator implements OutboundRequestDecorator
{

	private static final String X_CSRF_TOKEN = "X-CSRF-Token";
	private static final String X_CSRF_TOKEN_FETCH = "Fetch";
	private static final String X_CSRF_SET_COOKIES = "Set-Cookie";
	private static final String X_CSRF_COOKIE = "Cookie";
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCsrfOutboundRequestDecorator.class);
	private static final String CSRF_URL = "csrfURL";
	private static final String COOKIE_SEPARATOR = "; ";

	private IntegrationRestTemplateFactory integrationRestTemplateFactory;

	@Override
	public HttpEntity<Map<String, Object>> decorate(final HttpHeaders httpHeaders, final Map<String, Object> payload,
			final DecoratorContext context, final DecoratorExecution execution)
	{
		final ConsumedDestinationModel destinationModel = context.getDestinationModel();
		if (destinationModel.getAdditionalProperties().containsKey(CSRF_URL))
		{
			final CsrfParameters csrfParameters = fetchCsrfTokenFromSCPI(destinationModel);
			httpHeaders.set(X_CSRF_TOKEN, csrfParameters.getCsrfToken());
			httpHeaders.set(X_CSRF_COOKIE, csrfParameters.getCsrfCookie());
		}
		return execution.createHttpEntity(httpHeaders, payload, context);
	}

	protected CsrfParameters fetchCsrfTokenFromSCPI(final ConsumedDestinationModel destinationModel)
	{
		final HttpHeaders headers = setHeadersForTokenFetching();

		final RestOperations restOperations = getIntegrationRestTemplateFactory().create(destinationModel);
		final String tokenUrl = destinationModel.getAdditionalProperties().get(CSRF_URL);
		final ResponseEntity<Map> csrfTokenExchange = restOperations.exchange(tokenUrl, HttpMethod.GET, new HttpEntity(headers), Map.class);
		final String csrfTokenHeader = csrfTokenExchange.getHeaders().getFirst(X_CSRF_TOKEN);

		final List<String> csrfCookies = csrfTokenExchange.getHeaders().get(X_CSRF_SET_COOKIES);
		final String csrfCookieHeader = StringUtils.join(csrfCookies, COOKIE_SEPARATOR);

		if (!Strings.isNullOrEmpty(csrfTokenHeader) && !Strings.isNullOrEmpty(csrfCookieHeader))
		{
			LOG.debug("The CSRF token and cookies have been fetched from {}", destinationModel.getId());
		}
		else
		{
			throw new CSRFTokenFetchingException("Failed to fetch the CSRF token or CSRF cookies from " + destinationModel.getId());
		}

		return new CsrfParameters(csrfTokenHeader, csrfCookieHeader);
	}

	protected static HttpHeaders setHeadersForTokenFetching()
	{
		final HttpHeaders headers = new HttpHeaders();
		headers.set(X_CSRF_TOKEN, X_CSRF_TOKEN_FETCH);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		return headers;
	}

	protected IntegrationRestTemplateFactory getIntegrationRestTemplateFactory()
	{
		return integrationRestTemplateFactory;
	}

	@Required
	public void setIntegrationRestTemplateFactory(final IntegrationRestTemplateFactory integrationRestTemplateFactory)
	{
		this.integrationRestTemplateFactory = integrationRestTemplateFactory;
	}

	static class CsrfParameters
	{
		private final String csrfToken;
		private final String csrfCookie;

		public CsrfParameters(String csrfToken, String csrfCookie)
		{
			this.csrfToken = csrfToken;
			this.csrfCookie = csrfCookie;
		}

		public String getCsrfToken()
		{
			return csrfToken;
		}

		public String getCsrfCookie()
		{
			return csrfCookie;
		}
	}
}
