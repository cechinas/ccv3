/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices.cache.impl;

import de.hybris.platform.outboundservices.cache.DestinationRestTemplateCacheKey;
import de.hybris.platform.outboundservices.cache.RestTemplateCache;
import de.hybris.platform.regioncache.CacheValueLoader;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.region.CacheRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

public class DestinationRestTemplateCache implements RestTemplateCache
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DestinationRestTemplateCache.class);

	private CacheRegion cacheRegion;

	@Override
	public RestTemplate get(final DestinationRestTemplateCacheKey key)
	{
		if (key != null)
		{
			LOGGER.debug("Getting rest template for '{}'", key);
			final Object item = cacheRegion.get(internalKey(key));
			final RestTemplate restTemplate = item instanceof RestTemplate ? (RestTemplate) item : null;
			LOGGER.debug("Got rest template '{}' for destination '{}'", restTemplate, key.getDestinationId());
			return restTemplate;
		}
		LOGGER.warn("Not getting rest template because key is null");
		return null;
	}

	@Override
	public void put(final DestinationRestTemplateCacheKey key, final RestTemplate item)
	{
		if (key != null)
		{
			LOGGER.debug("Caching rest template for '{}'", key);
			cacheRegion.getWithLoader(internalKey(key), load(item));
		}
		else
		{
			LOGGER.warn("Not putting rest template into cache because key is null");
		}
	}

	@Override
	public RestTemplate remove(final DestinationRestTemplateCacheKey key)
	{
		if (key != null)
		{
			LOGGER.debug("Removing rest template for '{}'", key);
			final Object item = cacheRegion.invalidate(internalKey(key), false);
			final RestTemplate restTemplate = item instanceof RestTemplate ? (RestTemplate) item : null;
			LOGGER.debug("Removed rest template '{}' for destination '{}'", restTemplate, key.getDestinationId());
			return restTemplate;
		}
		LOGGER.warn("Not removing rest template because key is null");
		return null;
	}

	@Override
	public boolean contains(final DestinationRestTemplateCacheKey key)
	{
		return key != null && cacheRegion.containsKey(internalKey(key));
	}

	protected CacheKey internalKey(final DestinationRestTemplateCacheKey key)
	{
		return InternalDestinationRestTemplateCacheKey.from(key);
	}

	protected CacheValueLoader load(final RestTemplate item)
	{
		return RestTemplateCacheValueLoader.from(item);
	}

	@Required
	public void setCacheRegion(final CacheRegion cacheRegion)
	{
		this.cacheRegion = cacheRegion;
	}
}
