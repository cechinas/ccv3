/*
 * [y] hybris Platform
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.outboundsync.retry.impl;

import de.hybris.platform.outboundsync.config.impl.OutboundSyncConfiguration;
import de.hybris.platform.outboundsync.dto.OutboundItem;
import de.hybris.platform.outboundsync.dto.OutboundItemDTOGroup;
import de.hybris.platform.outboundsync.model.OutboundSyncRetryModel;
import de.hybris.platform.outboundsync.retry.RetrySearchService;
import de.hybris.platform.outboundsync.retry.RetryUpdateException;
import de.hybris.platform.outboundsync.retry.SyncRetryNotFoundException;
import de.hybris.platform.outboundsync.retry.SyncRetryService;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default implementation for updating the persisted retries based on the results of the synchronization process
 */
public class DefaultSyncRetryService implements SyncRetryService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSyncRetryService.class);

	private ModelService modelService;
	private OutboundSyncConfiguration outboundSyncConfiguration;
	private RetrySearchService retrySearchService;

	@Override
	public boolean determineLastAttemptAndUpdateRetry(final OutboundItemDTOGroup outboundItemDTOGroup)
	{
		try
		{
			final OutboundSyncRetryModel retry = findRetry(outboundItemDTOGroup);
			if (isMaxRetriesReached(retry))
			{
				markRetryAsMaxRetriesReached(retry);
				return true;
			}

			incrementRetryAttempt(retry);
			return false;
		}
		catch (final SyncRetryNotFoundException e)
		{
			LOG.trace("Retry not found", e);
			if (getOutboundSyncConfiguration().getMaxOutboundSyncRetries() <= 0)
			{
				return true;
			}
			createNewRetry(outboundItemDTOGroup);
			return false;
		}
	}

	private boolean isMaxRetriesReached(final OutboundSyncRetryModel retry)
	{
		return retry.getSyncAttempts() >= getOutboundSyncConfiguration().getMaxOutboundSyncRetries();
	}

	@Override
	public void handleSyncSuccess(final OutboundItemDTOGroup outboundItemDTOGroup)
	{
		try
		{
			final OutboundSyncRetryModel retry = findRetry(outboundItemDTOGroup);
			deleteRetry(retry);
		}
		catch (final SyncRetryNotFoundException e)
		{
			LOG.trace("Retry not found", e);
		}
	}

	private void deleteRetry(final OutboundSyncRetryModel retry)
	{
		try
		{
			getModelService().remove(retry.getPk());
		}
		catch (final ModelRemovalException e)
		{
			LOG.trace("The Retry was not removed", e);
			throw new RetryUpdateException(retry);
		}
	}

	/**
	 * Increments the retry attempt count by 1, and persists the update
	 *
	 * @param retry {@link OutboundSyncRetryModel} to update
	 */
	protected void incrementRetryAttempt(final OutboundSyncRetryModel retry)
	{
		retry.setSyncAttempts(retry.getSyncAttempts() + 1);
		updateRetry(retry);
	}

	/**
	 * Sets the {@link OutboundSyncRetryModel#setReachedMaxRetries(Boolean)} to true,
	 * and persists the update.
	 *
	 * @param retry {@link OutboundSyncRetryModel} to update
	 */
	protected void markRetryAsMaxRetriesReached(final OutboundSyncRetryModel retry)
	{
		retry.setReachedMaxRetries(true);
		updateRetry(retry);
	}

	/**
	 * Persists the given {@link OutboundSyncRetryModel}
	 *
	 * @param retry {@link OutboundSyncRetryModel} to save
	 */
	protected void updateRetry(final OutboundSyncRetryModel retry)
	{
		try
		{
			getModelService().save(retry);
		}
		catch (final ModelSavingException e)
		{
			LOG.trace("The Retry was not updated", e);
			throw new RetryUpdateException(retry);
		}
	}

	/**
	 * Creates a new retry record in the database
	 *
	 * @param outboundItemDTOGroup {@link OutboundItemDTOGroup} consists the information for creating the {@link OutboundSyncRetryModel}
	 */
	protected void createNewRetry(final OutboundItemDTOGroup outboundItemDTOGroup)
	{
		final OutboundSyncRetryModel retry = getModelService().create(OutboundSyncRetryModel.class);
		retry.setItemPk(outboundItemDTOGroup.getRootItemPk());
		retry.setChannel(outboundItemDTOGroup.getChannelConfiguration());
		retry.setSyncAttempts(1);

		getModelService().save(retry);
	}
	
	/**
	 * Searches the database for a {@link OutboundSyncRetryModel} that contains the information in the {@link OutboundItem}
	 *
	 * @param outboundItemDTOGroup {@link OutboundItemDTOGroup} consists the information for the search
	 * @return An OutboundSyncRetryModel if found
	 */
	protected OutboundSyncRetryModel findRetry(final OutboundItemDTOGroup outboundItemDTOGroup)
	{
		return getRetrySearchService().findRetry(outboundItemDTOGroup);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected OutboundSyncConfiguration getOutboundSyncConfiguration()
	{
		return outboundSyncConfiguration;
	}

	@Required
	public void setOutboundSyncConfiguration(final OutboundSyncConfiguration outboundSyncConfiguration)
	{
		this.outboundSyncConfiguration = outboundSyncConfiguration;
	}

	protected RetrySearchService getRetrySearchService()
	{
		return retrySearchService;
	}

	@Required
	public void setRetrySearchService(final RetrySearchService retrySearchService)
	{
		this.retrySearchService = retrySearchService;
	}
}
