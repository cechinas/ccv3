/*
 * [y] hybris Platform
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.outboundsync.retry;

import de.hybris.platform.outboundsync.dto.OutboundItemDTOGroup;

/**
 * Service responsible for updating the persisted retries based on the results of the synchronization process
 */
public interface SyncRetryService
{
	/**
	 * Performs the necessary operations in the retry table to handle a synchronization error {@link OutboundItemDTOGroup} and determines
	 * if it's the last attempt based on the configuration
	 *
	 * @param itemGroup with the information required for the search
	 * @return true if it's the last attempt, false if it is not.
	 */
	boolean determineLastAttemptAndUpdateRetry(OutboundItemDTOGroup itemGroup);

	/**
	 * Performs the necessary operations in the retry table to handle a successful synchronization
	 *
	 * @param itemGroup of type {@link OutboundItemDTOGroup} with information about the successful synchronization
	 */
	void handleSyncSuccess(OutboundItemDTOGroup itemGroup);
}
