/*
 * [y] hybris Platform
 *
 * Copyright (c) 2019 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.outboundsync.retry.impl

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.integrationservices.model.IntegrationObjectModel
import de.hybris.platform.outboundsync.config.impl.OutboundSyncConfiguration
import de.hybris.platform.outboundsync.dto.OutboundItemDTOGroup
import de.hybris.platform.outboundsync.model.OutboundChannelConfigurationModel
import de.hybris.platform.outboundsync.model.OutboundSyncRetryModel
import de.hybris.platform.outboundsync.retry.RetrySearchService
import de.hybris.platform.outboundsync.retry.RetryUpdateException
import de.hybris.platform.outboundsync.retry.SyncRetryNotFoundException
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException
import de.hybris.platform.servicelayer.exceptions.ModelSavingException
import de.hybris.platform.servicelayer.model.ModelService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultSyncRetryServiceUnitTest extends Specification {

	private static final int MAX_RETRIES = 3

	def retryService = new DefaultSyncRetryService()

	def modelService = Mock(ModelService)
	def outboundSyncConfiguration = Stub(OutboundSyncConfiguration)
	def retrySearchService = Mock(RetrySearchService)

	def setup() {
		retryService.setModelService(modelService)
		retryService.setOutboundSyncConfiguration(outboundSyncConfiguration)
		retryService.setRetrySearchService(retrySearchService)

		outboundSyncConfiguration.getMaxOutboundSyncRetries() >> MAX_RETRIES
	}

	@Test
	def "returns true when max retries is reached and retry is not deleted"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(MAX_RETRIES)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		def isLast = retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		1 * modelService.save(_) >> { args -> assert args[0].getReachedMaxRetries() }
		0 * modelService.remove(_)
		isLast
	}

	@Test
	def "returns false and increases count when retry exists and max retries is not reached"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(1)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		def isLast = retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		0 * modelService.remove(_)
		1 * modelService.save(_) >> { args ->
			assert args[0].getSyncAttempts() == 2
		}
		!isLast
	}

	@Test
	def "returns false and creates retry when retry does not exist"() {
		given:
		retrySearchService.findRetry(_) >> { throw new SyncRetryNotFoundException(123, "testChannel") }
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]
		modelService.create(_) >> new OutboundSyncRetryModel()

		when:
		def isLast = retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		0 * modelService.remove(_)
		1 * modelService.save(_) >> { args ->
			assert args[0].getSyncAttempts() == 1
			assert !args[0].getReachedMaxRetries()
		}
		!isLast
	}

	@Test
	def "retry is not deleted when it exists and max retries is less than one"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(2)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		def isLast = retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		outboundSyncConfiguration.getMaxOutboundSyncRetries() >> 0
		0 * modelService.remove(_)
		isLast
	}

	@Test
	def "returns true when max retries is less than one and retry is not created"() {
		given:
		retrySearchService.findRetry(_) >> { throw new SyncRetryNotFoundException(123, "testChannel") }
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		def isLast = retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		outboundSyncConfiguration.getMaxOutboundSyncRetries() >> 0
		0 * modelService.save(_)
		isLast
	}

	@Test
	def "retry is not created or removed when handling success"() {
		given:
		retrySearchService.findRetry(_) >> { throw new SyncRetryNotFoundException(123, "testChannel") }
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		retryService.handleSyncSuccess(outboundItemDTOGroup())

		then:
		0 * modelService.remove(_)
		0 * modelService.create(OutboundSyncRetryModel)
	}

	@Test
	def "retry is deleted when handling success and retry is found"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(1)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]

		when:
		retryService.handleSyncSuccess(outboundItemDTOGroup())

		then:
		1 * modelService.remove(_)
	}

	@Test
	def "exception is thrown when deleting retry that does not exist anymore"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(1)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]
		modelService.remove(_) >> { throw new ModelRemovalException("test error", new Throwable()) }

		when:
		retryService.handleSyncSuccess(outboundItemDTOGroup())

		then:
		thrown(RetryUpdateException)
	}

	@Test
	def "exception is thrown when updating retry that does not exist anymore"() {
		given:
		retrySearchService.findRetry(_) >> createRetry(1)
		modelService.get(_) >>> [Stub(IntegrationObjectModel), Stub(OutboundChannelConfigurationModel)]
		modelService.save(_) >> { throw new ModelSavingException("test error", new Throwable()) }

		when:
		retryService.determineLastAttemptAndUpdateRetry(outboundItemDTOGroup())

		then:
		thrown(RetryUpdateException)
	}

	private OutboundItemDTOGroup outboundItemDTOGroup() {
		Stub(OutboundItemDTOGroup) {
			getChannelConfiguration() >> Stub(OutboundChannelConfigurationModel)
			getRootItemPk() >> 123
		}
	}

	def createRetry(int attempts) {
		def retry = new OutboundSyncRetryModel()
		retry.setSyncAttempts(attempts)
		retry.setItemPk(123)
		retry.setChannel(Stub(OutboundChannelConfigurationModel))
		retry.setReachedMaxRetries(false)
		retry
	}
}
