/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.entitlementservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedEntitlementservicesConstants
{
	public static final String EXTENSIONNAME = "entitlementservices";
	public static class TC
	{
		public static final String ENTITLEMENT = "Entitlement".intern();
		public static final String ENTITLEMENTTIMEUNIT = "EntitlementTimeUnit".intern();
		public static final String PRODUCTENTITLEMENT = "ProductEntitlement".intern();
	}
	public static class Attributes
	{
		public static class Product
		{
			public static final String PRODUCTENTITLEMENTS = "productEntitlements".intern();
		}
	}
	public static class Enumerations
	{
		public static class EntitlementTimeUnit
		{
			public static final String DAY = "day".intern();
			public static final String MONTH = "month".intern();
		}
	}
	public static class Relations
	{
		public static final String PRODUCT2PRODUCTENTITLEMENTSRELATION = "Product2ProductEntitlementsRelation".intern();
		public static final String PRODUCTENTITLEMENT2ENTITLEMENTRELATION = "ProductEntitlement2EntitlementRelation".intern();
	}
	
	protected GeneratedEntitlementservicesConstants()
	{
		// private constructor
	}
	
	
}
