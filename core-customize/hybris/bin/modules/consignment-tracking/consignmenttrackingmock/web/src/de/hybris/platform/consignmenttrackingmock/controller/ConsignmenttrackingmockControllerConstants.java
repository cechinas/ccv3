/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.consignmenttrackingmock.controller;

/**
 *
 */
public class ConsignmenttrackingmockControllerConstants
{
	public interface Pages
	{
		String CONSIGNMENTMOCKPAGE = "/responsive/pages/consignment/mockTrackingPage";

		String MOCKCARRIERPAGE = "/responsive/pages/carrier/mockCarrierPage";
	}
}
