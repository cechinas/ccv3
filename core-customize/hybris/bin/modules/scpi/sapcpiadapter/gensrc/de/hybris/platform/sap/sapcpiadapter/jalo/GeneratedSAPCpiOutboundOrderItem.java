/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrderItem SAPCpiOutboundOrderItem}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundOrderItem extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.entryNumber</code> attribute **/
	public static final String ENTRYNUMBER = "entryNumber";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.quantity</code> attribute **/
	public static final String QUANTITY = "quantity";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.currencyIsoCode</code> attribute **/
	public static final String CURRENCYISOCODE = "currencyIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.unit</code> attribute **/
	public static final String UNIT = "unit";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.productCode</code> attribute **/
	public static final String PRODUCTCODE = "productCode";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.productName</code> attribute **/
	public static final String PRODUCTNAME = "productName";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.plant</code> attribute **/
	public static final String PLANT = "plant";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.namedDeliveryDate</code> attribute **/
	public static final String NAMEDDELIVERYDATE = "namedDeliveryDate";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.itemCategory</code> attribute **/
	public static final String ITEMCATEGORY = "itemCategory";
	/** Qualifier of the <code>SAPCpiOutboundOrderItem.sapCpiOutboundOrder</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDER = "sapCpiOutboundOrder";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundOrderItem> SAPCPIOUTBOUNDORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundOrderItem>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDORDERITEM,
	false,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(ENTRYNUMBER, AttributeMode.INITIAL);
		tmp.put(QUANTITY, AttributeMode.INITIAL);
		tmp.put(CURRENCYISOCODE, AttributeMode.INITIAL);
		tmp.put(UNIT, AttributeMode.INITIAL);
		tmp.put(PRODUCTCODE, AttributeMode.INITIAL);
		tmp.put(PRODUCTNAME, AttributeMode.INITIAL);
		tmp.put(PLANT, AttributeMode.INITIAL);
		tmp.put(NAMEDDELIVERYDATE, AttributeMode.INITIAL);
		tmp.put(ITEMCATEGORY, AttributeMode.INITIAL);
		tmp.put(SAPCPIOUTBOUNDORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CURRENCYISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode()
	{
		return getCurrencyIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CURRENCYISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final String value)
	{
		setCurrencyIsoCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENTRYNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber()
	{
		return getEntryNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENTRYNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final String value)
	{
		setEntryNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.itemCategory</code> attribute.
	 * @return the itemCategory
	 */
	public String getItemCategory(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ITEMCATEGORY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.itemCategory</code> attribute.
	 * @return the itemCategory
	 */
	public String getItemCategory()
	{
		return getItemCategory( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.itemCategory</code> attribute. 
	 * @param value the itemCategory
	 */
	public void setItemCategory(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ITEMCATEGORY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.itemCategory</code> attribute. 
	 * @param value the itemCategory
	 */
	public void setItemCategory(final String value)
	{
		setItemCategory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.namedDeliveryDate</code> attribute.
	 * @return the namedDeliveryDate
	 */
	public String getNamedDeliveryDate(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NAMEDDELIVERYDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.namedDeliveryDate</code> attribute.
	 * @return the namedDeliveryDate
	 */
	public String getNamedDeliveryDate()
	{
		return getNamedDeliveryDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.namedDeliveryDate</code> attribute. 
	 * @param value the namedDeliveryDate
	 */
	public void setNamedDeliveryDate(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NAMEDDELIVERYDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.namedDeliveryDate</code> attribute. 
	 * @param value the namedDeliveryDate
	 */
	public void setNamedDeliveryDate(final String value)
	{
		setNamedDeliveryDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.plant</code> attribute.
	 * @return the plant
	 */
	public String getPlant(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PLANT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.plant</code> attribute.
	 * @return the plant
	 */
	public String getPlant()
	{
		return getPlant( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.plant</code> attribute. 
	 * @param value the plant
	 */
	public void setPlant(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PLANT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.plant</code> attribute. 
	 * @param value the plant
	 */
	public void setPlant(final String value)
	{
		setPlant( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.productCode</code> attribute.
	 * @return the productCode
	 */
	public String getProductCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.productCode</code> attribute.
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return getProductCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.productCode</code> attribute. 
	 * @param value the productCode
	 */
	public void setProductCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.productCode</code> attribute. 
	 * @param value the productCode
	 */
	public void setProductCode(final String value)
	{
		setProductCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.productName</code> attribute.
	 * @return the productName
	 */
	public String getProductName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.productName</code> attribute.
	 * @return the productName
	 */
	public String getProductName()
	{
		return getProductName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.productName</code> attribute. 
	 * @param value the productName
	 */
	public void setProductName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.productName</code> attribute. 
	 * @param value the productName
	 */
	public void setProductName(final String value)
	{
		setProductName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.quantity</code> attribute.
	 * @return the quantity
	 */
	public String getQuantity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, QUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.quantity</code> attribute.
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return getQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, QUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final String value)
	{
		setQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder(final SessionContext ctx)
	{
		return (SAPCpiOutboundOrder)getProperty( ctx, SAPCPIOUTBOUNDORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder()
	{
		return getSapCpiOutboundOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SessionContext ctx, final SAPCpiOutboundOrder value)
	{
		SAPCPIOUTBOUNDORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SAPCpiOutboundOrder value)
	{
		setSapCpiOutboundOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.unit</code> attribute.
	 * @return the unit
	 */
	public String getUnit(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderItem.unit</code> attribute.
	 * @return the unit
	 */
	public String getUnit()
	{
		return getUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UNIT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderItem.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final String value)
	{
		setUnit( getSession().getSessionContext(), value );
	}
	
}
