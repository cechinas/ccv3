/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPriceComponent SAPCpiOutboundPriceComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundPriceComponent extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.entryNumber</code> attribute **/
	public static final String ENTRYNUMBER = "entryNumber";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.value</code> attribute **/
	public static final String VALUE = "value";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.unit</code> attribute **/
	public static final String UNIT = "unit";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.absolute</code> attribute **/
	public static final String ABSOLUTE = "absolute";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.conditionCode</code> attribute **/
	public static final String CONDITIONCODE = "conditionCode";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.conditionCounter</code> attribute **/
	public static final String CONDITIONCOUNTER = "conditionCounter";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.currencyIsoCode</code> attribute **/
	public static final String CURRENCYISOCODE = "currencyIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.priceQuantity</code> attribute **/
	public static final String PRICEQUANTITY = "priceQuantity";
	/** Qualifier of the <code>SAPCpiOutboundPriceComponent.sapCpiOutboundOrder</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDER = "sapCpiOutboundOrder";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundPriceComponent> SAPCPIOUTBOUNDORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundPriceComponent>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPRICECOMPONENT,
	false,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(ENTRYNUMBER, AttributeMode.INITIAL);
		tmp.put(VALUE, AttributeMode.INITIAL);
		tmp.put(UNIT, AttributeMode.INITIAL);
		tmp.put(ABSOLUTE, AttributeMode.INITIAL);
		tmp.put(CONDITIONCODE, AttributeMode.INITIAL);
		tmp.put(CONDITIONCOUNTER, AttributeMode.INITIAL);
		tmp.put(CURRENCYISOCODE, AttributeMode.INITIAL);
		tmp.put(PRICEQUANTITY, AttributeMode.INITIAL);
		tmp.put(SAPCPIOUTBOUNDORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.absolute</code> attribute.
	 * @return the absolute
	 */
	public String getAbsolute(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ABSOLUTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.absolute</code> attribute.
	 * @return the absolute
	 */
	public String getAbsolute()
	{
		return getAbsolute( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.absolute</code> attribute. 
	 * @param value the absolute
	 */
	public void setAbsolute(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ABSOLUTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.absolute</code> attribute. 
	 * @param value the absolute
	 */
	public void setAbsolute(final String value)
	{
		setAbsolute( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.conditionCode</code> attribute.
	 * @return the conditionCode
	 */
	public String getConditionCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CONDITIONCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.conditionCode</code> attribute.
	 * @return the conditionCode
	 */
	public String getConditionCode()
	{
		return getConditionCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.conditionCode</code> attribute. 
	 * @param value the conditionCode
	 */
	public void setConditionCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CONDITIONCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.conditionCode</code> attribute. 
	 * @param value the conditionCode
	 */
	public void setConditionCode(final String value)
	{
		setConditionCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.conditionCounter</code> attribute.
	 * @return the conditionCounter
	 */
	public String getConditionCounter(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CONDITIONCOUNTER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.conditionCounter</code> attribute.
	 * @return the conditionCounter
	 */
	public String getConditionCounter()
	{
		return getConditionCounter( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.conditionCounter</code> attribute. 
	 * @param value the conditionCounter
	 */
	public void setConditionCounter(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CONDITIONCOUNTER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.conditionCounter</code> attribute. 
	 * @param value the conditionCounter
	 */
	public void setConditionCounter(final String value)
	{
		setConditionCounter( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CURRENCYISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode()
	{
		return getCurrencyIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CURRENCYISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final String value)
	{
		setCurrencyIsoCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENTRYNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber()
	{
		return getEntryNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENTRYNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final String value)
	{
		setEntryNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.priceQuantity</code> attribute.
	 * @return the priceQuantity
	 */
	public String getPriceQuantity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRICEQUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.priceQuantity</code> attribute.
	 * @return the priceQuantity
	 */
	public String getPriceQuantity()
	{
		return getPriceQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.priceQuantity</code> attribute. 
	 * @param value the priceQuantity
	 */
	public void setPriceQuantity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRICEQUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.priceQuantity</code> attribute. 
	 * @param value the priceQuantity
	 */
	public void setPriceQuantity(final String value)
	{
		setPriceQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder(final SessionContext ctx)
	{
		return (SAPCpiOutboundOrder)getProperty( ctx, SAPCPIOUTBOUNDORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder()
	{
		return getSapCpiOutboundOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SessionContext ctx, final SAPCpiOutboundOrder value)
	{
		SAPCPIOUTBOUNDORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SAPCpiOutboundOrder value)
	{
		setSapCpiOutboundOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.unit</code> attribute.
	 * @return the unit
	 */
	public String getUnit(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.unit</code> attribute.
	 * @return the unit
	 */
	public String getUnit()
	{
		return getUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UNIT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final String value)
	{
		setUnit( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.value</code> attribute.
	 * @return the value
	 */
	public String getValue(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VALUE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPriceComponent.value</code> attribute.
	 * @return the value
	 */
	public String getValue()
	{
		return getValue( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.value</code> attribute. 
	 * @param value the value
	 */
	public void setValue(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VALUE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPriceComponent.value</code> attribute. 
	 * @param value the value
	 */
	public void setValue(final String value)
	{
		setValue( getSession().getSessionContext(), value );
	}
	
}
