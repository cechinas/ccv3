/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundAddress;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCardPayment;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundConfig;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrderItem;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPartnerRole;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPriceComponent;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder SAPCpiOutboundOrder}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundOrder extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundOrder.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundOrder.creationDate</code> attribute **/
	public static final String CREATIONDATE = "creationDate";
	/** Qualifier of the <code>SAPCpiOutboundOrder.currencyIsoCode</code> attribute **/
	public static final String CURRENCYISOCODE = "currencyIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundOrder.paymentMode</code> attribute **/
	public static final String PAYMENTMODE = "paymentMode";
	/** Qualifier of the <code>SAPCpiOutboundOrder.deliveryMode</code> attribute **/
	public static final String DELIVERYMODE = "deliveryMode";
	/** Qualifier of the <code>SAPCpiOutboundOrder.purchaseOrderNumber</code> attribute **/
	public static final String PURCHASEORDERNUMBER = "purchaseOrderNumber";
	/** Qualifier of the <code>SAPCpiOutboundOrder.baseStoreUid</code> attribute **/
	public static final String BASESTOREUID = "baseStoreUid";
	/** Qualifier of the <code>SAPCpiOutboundOrder.channel</code> attribute **/
	public static final String CHANNEL = "channel";
	/** Qualifier of the <code>SAPCpiOutboundOrder.salesOrganization</code> attribute **/
	public static final String SALESORGANIZATION = "salesOrganization";
	/** Qualifier of the <code>SAPCpiOutboundOrder.distributionChannel</code> attribute **/
	public static final String DISTRIBUTIONCHANNEL = "distributionChannel";
	/** Qualifier of the <code>SAPCpiOutboundOrder.division</code> attribute **/
	public static final String DIVISION = "division";
	/** Qualifier of the <code>SAPCpiOutboundOrder.transactionType</code> attribute **/
	public static final String TRANSACTIONTYPE = "transactionType";
	/** Qualifier of the <code>SAPCpiOutboundOrder.shippingCondition</code> attribute **/
	public static final String SHIPPINGCONDITION = "shippingCondition";
	/** Qualifier of the <code>SAPCpiOutboundOrder.responseStatus</code> attribute **/
	public static final String RESPONSESTATUS = "responseStatus";
	/** Qualifier of the <code>SAPCpiOutboundOrder.responseMessage</code> attribute **/
	public static final String RESPONSEMESSAGE = "responseMessage";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiConfig</code> attribute **/
	public static final String SAPCPICONFIG = "sapCpiConfig";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiOutboundOrderItems</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDERITEMS = "sapCpiOutboundOrderItems";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiOutboundPartnerRoles</code> attribute **/
	public static final String SAPCPIOUTBOUNDPARTNERROLES = "sapCpiOutboundPartnerRoles";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiOutboundCardPayments</code> attribute **/
	public static final String SAPCPIOUTBOUNDCARDPAYMENTS = "sapCpiOutboundCardPayments";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiOutboundPriceComponents</code> attribute **/
	public static final String SAPCPIOUTBOUNDPRICECOMPONENTS = "sapCpiOutboundPriceComponents";
	/** Qualifier of the <code>SAPCpiOutboundOrder.sapCpiOutboundAddresses</code> attribute **/
	public static final String SAPCPIOUTBOUNDADDRESSES = "sapCpiOutboundAddresses";
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDERITEMS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundOrderItem> SAPCPIOUTBOUNDORDERITEMSHANDLER = new OneToManyHandler<SAPCpiOutboundOrderItem>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDORDERITEM,
	true,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundOrderItems");
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDPARTNERROLES's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundPartnerRole> SAPCPIOUTBOUNDPARTNERROLESHANDLER = new OneToManyHandler<SAPCpiOutboundPartnerRole>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPARTNERROLE,
	true,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundPartnerRoles");
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDCARDPAYMENTS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundCardPayment> SAPCPIOUTBOUNDCARDPAYMENTSHANDLER = new OneToManyHandler<SAPCpiOutboundCardPayment>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDCARDPAYMENT,
	true,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundCardPayments");
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDPRICECOMPONENTS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundPriceComponent> SAPCPIOUTBOUNDPRICECOMPONENTSHANDLER = new OneToManyHandler<SAPCpiOutboundPriceComponent>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPRICECOMPONENT,
	true,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundPriceComponents");
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDADDRESSES's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundAddress> SAPCPIOUTBOUNDADDRESSESHANDLER = new OneToManyHandler<SAPCpiOutboundAddress>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDADDRESS,
	true,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundAddresses");
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(CREATIONDATE, AttributeMode.INITIAL);
		tmp.put(CURRENCYISOCODE, AttributeMode.INITIAL);
		tmp.put(PAYMENTMODE, AttributeMode.INITIAL);
		tmp.put(DELIVERYMODE, AttributeMode.INITIAL);
		tmp.put(PURCHASEORDERNUMBER, AttributeMode.INITIAL);
		tmp.put(BASESTOREUID, AttributeMode.INITIAL);
		tmp.put(CHANNEL, AttributeMode.INITIAL);
		tmp.put(SALESORGANIZATION, AttributeMode.INITIAL);
		tmp.put(DISTRIBUTIONCHANNEL, AttributeMode.INITIAL);
		tmp.put(DIVISION, AttributeMode.INITIAL);
		tmp.put(TRANSACTIONTYPE, AttributeMode.INITIAL);
		tmp.put(SHIPPINGCONDITION, AttributeMode.INITIAL);
		tmp.put(RESPONSESTATUS, AttributeMode.INITIAL);
		tmp.put(RESPONSEMESSAGE, AttributeMode.INITIAL);
		tmp.put(SAPCPICONFIG, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.baseStoreUid</code> attribute.
	 * @return the baseStoreUid
	 */
	public String getBaseStoreUid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BASESTOREUID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.baseStoreUid</code> attribute.
	 * @return the baseStoreUid
	 */
	public String getBaseStoreUid()
	{
		return getBaseStoreUid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.baseStoreUid</code> attribute. 
	 * @param value the baseStoreUid
	 */
	public void setBaseStoreUid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BASESTOREUID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.baseStoreUid</code> attribute. 
	 * @param value the baseStoreUid
	 */
	public void setBaseStoreUid(final String value)
	{
		setBaseStoreUid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.channel</code> attribute.
	 * @return the channel
	 */
	public String getChannel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CHANNEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.channel</code> attribute.
	 * @return the channel
	 */
	public String getChannel()
	{
		return getChannel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.channel</code> attribute. 
	 * @param value the channel
	 */
	public void setChannel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CHANNEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.channel</code> attribute. 
	 * @param value the channel
	 */
	public void setChannel(final String value)
	{
		setChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.creationDate</code> attribute.
	 * @return the creationDate
	 */
	public String getCreationDate(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CREATIONDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.creationDate</code> attribute.
	 * @return the creationDate
	 */
	public String getCreationDate()
	{
		return getCreationDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.creationDate</code> attribute. 
	 * @param value the creationDate
	 */
	public void setCreationDate(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CREATIONDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.creationDate</code> attribute. 
	 * @param value the creationDate
	 */
	public void setCreationDate(final String value)
	{
		setCreationDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CURRENCYISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.currencyIsoCode</code> attribute.
	 * @return the currencyIsoCode
	 */
	public String getCurrencyIsoCode()
	{
		return getCurrencyIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CURRENCYISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.currencyIsoCode</code> attribute. 
	 * @param value the currencyIsoCode
	 */
	public void setCurrencyIsoCode(final String value)
	{
		setCurrencyIsoCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.deliveryMode</code> attribute.
	 * @return the deliveryMode
	 */
	public String getDeliveryMode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DELIVERYMODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.deliveryMode</code> attribute.
	 * @return the deliveryMode
	 */
	public String getDeliveryMode()
	{
		return getDeliveryMode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.deliveryMode</code> attribute. 
	 * @param value the deliveryMode
	 */
	public void setDeliveryMode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DELIVERYMODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.deliveryMode</code> attribute. 
	 * @param value the deliveryMode
	 */
	public void setDeliveryMode(final String value)
	{
		setDeliveryMode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.distributionChannel</code> attribute.
	 * @return the distributionChannel
	 */
	public String getDistributionChannel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DISTRIBUTIONCHANNEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.distributionChannel</code> attribute.
	 * @return the distributionChannel
	 */
	public String getDistributionChannel()
	{
		return getDistributionChannel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.distributionChannel</code> attribute. 
	 * @param value the distributionChannel
	 */
	public void setDistributionChannel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DISTRIBUTIONCHANNEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.distributionChannel</code> attribute. 
	 * @param value the distributionChannel
	 */
	public void setDistributionChannel(final String value)
	{
		setDistributionChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.division</code> attribute.
	 * @return the division
	 */
	public String getDivision(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIVISION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.division</code> attribute.
	 * @return the division
	 */
	public String getDivision()
	{
		return getDivision( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.division</code> attribute. 
	 * @param value the division
	 */
	public void setDivision(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIVISION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.division</code> attribute. 
	 * @param value the division
	 */
	public void setDivision(final String value)
	{
		setDivision( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.paymentMode</code> attribute.
	 * @return the paymentMode
	 */
	public String getPaymentMode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PAYMENTMODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.paymentMode</code> attribute.
	 * @return the paymentMode
	 */
	public String getPaymentMode()
	{
		return getPaymentMode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.paymentMode</code> attribute. 
	 * @param value the paymentMode
	 */
	public void setPaymentMode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PAYMENTMODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.paymentMode</code> attribute. 
	 * @param value the paymentMode
	 */
	public void setPaymentMode(final String value)
	{
		setPaymentMode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.purchaseOrderNumber</code> attribute.
	 * @return the purchaseOrderNumber
	 */
	public String getPurchaseOrderNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PURCHASEORDERNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.purchaseOrderNumber</code> attribute.
	 * @return the purchaseOrderNumber
	 */
	public String getPurchaseOrderNumber()
	{
		return getPurchaseOrderNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.purchaseOrderNumber</code> attribute. 
	 * @param value the purchaseOrderNumber
	 */
	public void setPurchaseOrderNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PURCHASEORDERNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.purchaseOrderNumber</code> attribute. 
	 * @param value the purchaseOrderNumber
	 */
	public void setPurchaseOrderNumber(final String value)
	{
		setPurchaseOrderNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.responseMessage</code> attribute.
	 * @return the responseMessage
	 */
	public String getResponseMessage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RESPONSEMESSAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.responseMessage</code> attribute.
	 * @return the responseMessage
	 */
	public String getResponseMessage()
	{
		return getResponseMessage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.responseMessage</code> attribute. 
	 * @param value the responseMessage
	 */
	public void setResponseMessage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RESPONSEMESSAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.responseMessage</code> attribute. 
	 * @param value the responseMessage
	 */
	public void setResponseMessage(final String value)
	{
		setResponseMessage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.responseStatus</code> attribute.
	 * @return the responseStatus
	 */
	public String getResponseStatus(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RESPONSESTATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.responseStatus</code> attribute.
	 * @return the responseStatus
	 */
	public String getResponseStatus()
	{
		return getResponseStatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.responseStatus</code> attribute. 
	 * @param value the responseStatus
	 */
	public void setResponseStatus(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RESPONSESTATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.responseStatus</code> attribute. 
	 * @param value the responseStatus
	 */
	public void setResponseStatus(final String value)
	{
		setResponseStatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.salesOrganization</code> attribute.
	 * @return the salesOrganization
	 */
	public String getSalesOrganization(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESORGANIZATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.salesOrganization</code> attribute.
	 * @return the salesOrganization
	 */
	public String getSalesOrganization()
	{
		return getSalesOrganization( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.salesOrganization</code> attribute. 
	 * @param value the salesOrganization
	 */
	public void setSalesOrganization(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESORGANIZATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.salesOrganization</code> attribute. 
	 * @param value the salesOrganization
	 */
	public void setSalesOrganization(final String value)
	{
		setSalesOrganization( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiConfig</code> attribute.
	 * @return the sapCpiConfig
	 */
	public SAPCpiOutboundConfig getSapCpiConfig(final SessionContext ctx)
	{
		return (SAPCpiOutboundConfig)getProperty( ctx, SAPCPICONFIG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiConfig</code> attribute.
	 * @return the sapCpiConfig
	 */
	public SAPCpiOutboundConfig getSapCpiConfig()
	{
		return getSapCpiConfig( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiConfig</code> attribute. 
	 * @param value the sapCpiConfig
	 */
	public void setSapCpiConfig(final SessionContext ctx, final SAPCpiOutboundConfig value)
	{
		setProperty(ctx, SAPCPICONFIG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiConfig</code> attribute. 
	 * @param value the sapCpiConfig
	 */
	public void setSapCpiConfig(final SAPCpiOutboundConfig value)
	{
		setSapCpiConfig( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundAddresses</code> attribute.
	 * @return the sapCpiOutboundAddresses
	 */
	public Set<SAPCpiOutboundAddress> getSapCpiOutboundAddresses(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundAddress>)SAPCPIOUTBOUNDADDRESSESHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundAddresses</code> attribute.
	 * @return the sapCpiOutboundAddresses
	 */
	public Set<SAPCpiOutboundAddress> getSapCpiOutboundAddresses()
	{
		return getSapCpiOutboundAddresses( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundAddresses</code> attribute. 
	 * @param value the sapCpiOutboundAddresses
	 */
	public void setSapCpiOutboundAddresses(final SessionContext ctx, final Set<SAPCpiOutboundAddress> value)
	{
		SAPCPIOUTBOUNDADDRESSESHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundAddresses</code> attribute. 
	 * @param value the sapCpiOutboundAddresses
	 */
	public void setSapCpiOutboundAddresses(final Set<SAPCpiOutboundAddress> value)
	{
		setSapCpiOutboundAddresses( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundAddresses. 
	 * @param value the item to add to sapCpiOutboundAddresses
	 */
	public void addToSapCpiOutboundAddresses(final SessionContext ctx, final SAPCpiOutboundAddress value)
	{
		SAPCPIOUTBOUNDADDRESSESHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundAddresses. 
	 * @param value the item to add to sapCpiOutboundAddresses
	 */
	public void addToSapCpiOutboundAddresses(final SAPCpiOutboundAddress value)
	{
		addToSapCpiOutboundAddresses( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundAddresses. 
	 * @param value the item to remove from sapCpiOutboundAddresses
	 */
	public void removeFromSapCpiOutboundAddresses(final SessionContext ctx, final SAPCpiOutboundAddress value)
	{
		SAPCPIOUTBOUNDADDRESSESHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundAddresses. 
	 * @param value the item to remove from sapCpiOutboundAddresses
	 */
	public void removeFromSapCpiOutboundAddresses(final SAPCpiOutboundAddress value)
	{
		removeFromSapCpiOutboundAddresses( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundCardPayments</code> attribute.
	 * @return the sapCpiOutboundCardPayments
	 */
	public Set<SAPCpiOutboundCardPayment> getSapCpiOutboundCardPayments(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundCardPayment>)SAPCPIOUTBOUNDCARDPAYMENTSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundCardPayments</code> attribute.
	 * @return the sapCpiOutboundCardPayments
	 */
	public Set<SAPCpiOutboundCardPayment> getSapCpiOutboundCardPayments()
	{
		return getSapCpiOutboundCardPayments( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundCardPayments</code> attribute. 
	 * @param value the sapCpiOutboundCardPayments
	 */
	public void setSapCpiOutboundCardPayments(final SessionContext ctx, final Set<SAPCpiOutboundCardPayment> value)
	{
		SAPCPIOUTBOUNDCARDPAYMENTSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundCardPayments</code> attribute. 
	 * @param value the sapCpiOutboundCardPayments
	 */
	public void setSapCpiOutboundCardPayments(final Set<SAPCpiOutboundCardPayment> value)
	{
		setSapCpiOutboundCardPayments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundCardPayments. 
	 * @param value the item to add to sapCpiOutboundCardPayments
	 */
	public void addToSapCpiOutboundCardPayments(final SessionContext ctx, final SAPCpiOutboundCardPayment value)
	{
		SAPCPIOUTBOUNDCARDPAYMENTSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundCardPayments. 
	 * @param value the item to add to sapCpiOutboundCardPayments
	 */
	public void addToSapCpiOutboundCardPayments(final SAPCpiOutboundCardPayment value)
	{
		addToSapCpiOutboundCardPayments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundCardPayments. 
	 * @param value the item to remove from sapCpiOutboundCardPayments
	 */
	public void removeFromSapCpiOutboundCardPayments(final SessionContext ctx, final SAPCpiOutboundCardPayment value)
	{
		SAPCPIOUTBOUNDCARDPAYMENTSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundCardPayments. 
	 * @param value the item to remove from sapCpiOutboundCardPayments
	 */
	public void removeFromSapCpiOutboundCardPayments(final SAPCpiOutboundCardPayment value)
	{
		removeFromSapCpiOutboundCardPayments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundOrderItems</code> attribute.
	 * @return the sapCpiOutboundOrderItems
	 */
	public Set<SAPCpiOutboundOrderItem> getSapCpiOutboundOrderItems(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundOrderItem>)SAPCPIOUTBOUNDORDERITEMSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundOrderItems</code> attribute.
	 * @return the sapCpiOutboundOrderItems
	 */
	public Set<SAPCpiOutboundOrderItem> getSapCpiOutboundOrderItems()
	{
		return getSapCpiOutboundOrderItems( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundOrderItems</code> attribute. 
	 * @param value the sapCpiOutboundOrderItems
	 */
	public void setSapCpiOutboundOrderItems(final SessionContext ctx, final Set<SAPCpiOutboundOrderItem> value)
	{
		SAPCPIOUTBOUNDORDERITEMSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundOrderItems</code> attribute. 
	 * @param value the sapCpiOutboundOrderItems
	 */
	public void setSapCpiOutboundOrderItems(final Set<SAPCpiOutboundOrderItem> value)
	{
		setSapCpiOutboundOrderItems( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundOrderItems. 
	 * @param value the item to add to sapCpiOutboundOrderItems
	 */
	public void addToSapCpiOutboundOrderItems(final SessionContext ctx, final SAPCpiOutboundOrderItem value)
	{
		SAPCPIOUTBOUNDORDERITEMSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundOrderItems. 
	 * @param value the item to add to sapCpiOutboundOrderItems
	 */
	public void addToSapCpiOutboundOrderItems(final SAPCpiOutboundOrderItem value)
	{
		addToSapCpiOutboundOrderItems( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundOrderItems. 
	 * @param value the item to remove from sapCpiOutboundOrderItems
	 */
	public void removeFromSapCpiOutboundOrderItems(final SessionContext ctx, final SAPCpiOutboundOrderItem value)
	{
		SAPCPIOUTBOUNDORDERITEMSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundOrderItems. 
	 * @param value the item to remove from sapCpiOutboundOrderItems
	 */
	public void removeFromSapCpiOutboundOrderItems(final SAPCpiOutboundOrderItem value)
	{
		removeFromSapCpiOutboundOrderItems( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPartnerRoles</code> attribute.
	 * @return the sapCpiOutboundPartnerRoles
	 */
	public Set<SAPCpiOutboundPartnerRole> getSapCpiOutboundPartnerRoles(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundPartnerRole>)SAPCPIOUTBOUNDPARTNERROLESHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPartnerRoles</code> attribute.
	 * @return the sapCpiOutboundPartnerRoles
	 */
	public Set<SAPCpiOutboundPartnerRole> getSapCpiOutboundPartnerRoles()
	{
		return getSapCpiOutboundPartnerRoles( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPartnerRoles</code> attribute. 
	 * @param value the sapCpiOutboundPartnerRoles
	 */
	public void setSapCpiOutboundPartnerRoles(final SessionContext ctx, final Set<SAPCpiOutboundPartnerRole> value)
	{
		SAPCPIOUTBOUNDPARTNERROLESHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPartnerRoles</code> attribute. 
	 * @param value the sapCpiOutboundPartnerRoles
	 */
	public void setSapCpiOutboundPartnerRoles(final Set<SAPCpiOutboundPartnerRole> value)
	{
		setSapCpiOutboundPartnerRoles( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundPartnerRoles. 
	 * @param value the item to add to sapCpiOutboundPartnerRoles
	 */
	public void addToSapCpiOutboundPartnerRoles(final SessionContext ctx, final SAPCpiOutboundPartnerRole value)
	{
		SAPCPIOUTBOUNDPARTNERROLESHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundPartnerRoles. 
	 * @param value the item to add to sapCpiOutboundPartnerRoles
	 */
	public void addToSapCpiOutboundPartnerRoles(final SAPCpiOutboundPartnerRole value)
	{
		addToSapCpiOutboundPartnerRoles( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundPartnerRoles. 
	 * @param value the item to remove from sapCpiOutboundPartnerRoles
	 */
	public void removeFromSapCpiOutboundPartnerRoles(final SessionContext ctx, final SAPCpiOutboundPartnerRole value)
	{
		SAPCPIOUTBOUNDPARTNERROLESHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundPartnerRoles. 
	 * @param value the item to remove from sapCpiOutboundPartnerRoles
	 */
	public void removeFromSapCpiOutboundPartnerRoles(final SAPCpiOutboundPartnerRole value)
	{
		removeFromSapCpiOutboundPartnerRoles( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPriceComponents</code> attribute.
	 * @return the sapCpiOutboundPriceComponents
	 */
	public Set<SAPCpiOutboundPriceComponent> getSapCpiOutboundPriceComponents(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundPriceComponent>)SAPCPIOUTBOUNDPRICECOMPONENTSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPriceComponents</code> attribute.
	 * @return the sapCpiOutboundPriceComponents
	 */
	public Set<SAPCpiOutboundPriceComponent> getSapCpiOutboundPriceComponents()
	{
		return getSapCpiOutboundPriceComponents( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPriceComponents</code> attribute. 
	 * @param value the sapCpiOutboundPriceComponents
	 */
	public void setSapCpiOutboundPriceComponents(final SessionContext ctx, final Set<SAPCpiOutboundPriceComponent> value)
	{
		SAPCPIOUTBOUNDPRICECOMPONENTSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.sapCpiOutboundPriceComponents</code> attribute. 
	 * @param value the sapCpiOutboundPriceComponents
	 */
	public void setSapCpiOutboundPriceComponents(final Set<SAPCpiOutboundPriceComponent> value)
	{
		setSapCpiOutboundPriceComponents( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundPriceComponents. 
	 * @param value the item to add to sapCpiOutboundPriceComponents
	 */
	public void addToSapCpiOutboundPriceComponents(final SessionContext ctx, final SAPCpiOutboundPriceComponent value)
	{
		SAPCPIOUTBOUNDPRICECOMPONENTSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundPriceComponents. 
	 * @param value the item to add to sapCpiOutboundPriceComponents
	 */
	public void addToSapCpiOutboundPriceComponents(final SAPCpiOutboundPriceComponent value)
	{
		addToSapCpiOutboundPriceComponents( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundPriceComponents. 
	 * @param value the item to remove from sapCpiOutboundPriceComponents
	 */
	public void removeFromSapCpiOutboundPriceComponents(final SessionContext ctx, final SAPCpiOutboundPriceComponent value)
	{
		SAPCPIOUTBOUNDPRICECOMPONENTSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundPriceComponents. 
	 * @param value the item to remove from sapCpiOutboundPriceComponents
	 */
	public void removeFromSapCpiOutboundPriceComponents(final SAPCpiOutboundPriceComponent value)
	{
		removeFromSapCpiOutboundPriceComponents( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.shippingCondition</code> attribute.
	 * @return the shippingCondition
	 */
	public String getShippingCondition(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPINGCONDITION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.shippingCondition</code> attribute.
	 * @return the shippingCondition
	 */
	public String getShippingCondition()
	{
		return getShippingCondition( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.shippingCondition</code> attribute. 
	 * @param value the shippingCondition
	 */
	public void setShippingCondition(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPINGCONDITION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.shippingCondition</code> attribute. 
	 * @param value the shippingCondition
	 */
	public void setShippingCondition(final String value)
	{
		setShippingCondition( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.transactionType</code> attribute.
	 * @return the transactionType
	 */
	public String getTransactionType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSACTIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrder.transactionType</code> attribute.
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return getTransactionType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.transactionType</code> attribute. 
	 * @param value the transactionType
	 */
	public void setTransactionType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSACTIONTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrder.transactionType</code> attribute. 
	 * @param value the transactionType
	 */
	public void setTransactionType(final String value)
	{
		setTransactionType( getSession().getSessionContext(), value );
	}
	
}
