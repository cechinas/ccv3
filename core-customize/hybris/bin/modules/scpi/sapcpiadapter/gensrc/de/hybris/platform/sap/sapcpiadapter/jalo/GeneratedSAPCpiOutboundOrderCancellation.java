/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrderCancellation SAPCpiOutboundOrderCancellation}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundOrderCancellation extends SAPCpiOutboundOrder
{
	/** Qualifier of the <code>SAPCpiOutboundOrderCancellation.rejectionReason</code> attribute **/
	public static final String REJECTIONREASON = "rejectionReason";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SAPCpiOutboundOrder.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(REJECTIONREASON, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderCancellation.rejectionReason</code> attribute.
	 * @return the rejectionReason
	 */
	public String getRejectionReason(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REJECTIONREASON);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundOrderCancellation.rejectionReason</code> attribute.
	 * @return the rejectionReason
	 */
	public String getRejectionReason()
	{
		return getRejectionReason( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderCancellation.rejectionReason</code> attribute. 
	 * @param value the rejectionReason
	 */
	public void setRejectionReason(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REJECTIONREASON,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundOrderCancellation.rejectionReason</code> attribute. 
	 * @param value the rejectionReason
	 */
	public void setRejectionReason(final String value)
	{
		setRejectionReason( getSession().getSessionContext(), value );
	}
	
}
