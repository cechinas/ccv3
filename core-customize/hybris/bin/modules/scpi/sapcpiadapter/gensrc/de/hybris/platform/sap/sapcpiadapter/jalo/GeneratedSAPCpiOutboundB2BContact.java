/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BCustomer;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BContact SAPCpiOutboundB2BContact}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundB2BContact extends SAPCpiOutboundB2BCustomer
{
	/** Qualifier of the <code>SAPCpiOutboundB2BContact.sapCpiOutboundB2BCustomer</code> attribute **/
	public static final String SAPCPIOUTBOUNDB2BCUSTOMER = "sapCpiOutboundB2BCustomer";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDB2BCUSTOMER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundB2BContact> SAPCPIOUTBOUNDB2BCUSTOMERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundB2BContact>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDB2BCONTACT,
	false,
	"sapCpiOutboundB2BCustomer",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SAPCpiOutboundB2BCustomer.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(SAPCPIOUTBOUNDB2BCUSTOMER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDB2BCUSTOMERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BContact.sapCpiOutboundB2BCustomer</code> attribute.
	 * @return the sapCpiOutboundB2BCustomer
	 */
	public SAPCpiOutboundB2BCustomer getSapCpiOutboundB2BCustomer(final SessionContext ctx)
	{
		return (SAPCpiOutboundB2BCustomer)getProperty( ctx, SAPCPIOUTBOUNDB2BCUSTOMER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BContact.sapCpiOutboundB2BCustomer</code> attribute.
	 * @return the sapCpiOutboundB2BCustomer
	 */
	public SAPCpiOutboundB2BCustomer getSapCpiOutboundB2BCustomer()
	{
		return getSapCpiOutboundB2BCustomer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BContact.sapCpiOutboundB2BCustomer</code> attribute. 
	 * @param value the sapCpiOutboundB2BCustomer
	 */
	public void setSapCpiOutboundB2BCustomer(final SessionContext ctx, final SAPCpiOutboundB2BCustomer value)
	{
		SAPCPIOUTBOUNDB2BCUSTOMERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BContact.sapCpiOutboundB2BCustomer</code> attribute. 
	 * @param value the sapCpiOutboundB2BCustomer
	 */
	public void setSapCpiOutboundB2BCustomer(final SAPCpiOutboundB2BCustomer value)
	{
		setSapCpiOutboundB2BCustomer( getSession().getSessionContext(), value );
	}
	
}
