/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundConfig SAPCpiOutboundConfig}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundConfig extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundConfig.url</code> attribute **/
	public static final String URL = "url";
	/** Qualifier of the <code>SAPCpiOutboundConfig.username</code> attribute **/
	public static final String USERNAME = "username";
	/** Qualifier of the <code>SAPCpiOutboundConfig.senderName</code> attribute **/
	public static final String SENDERNAME = "senderName";
	/** Qualifier of the <code>SAPCpiOutboundConfig.senderPort</code> attribute **/
	public static final String SENDERPORT = "senderPort";
	/** Qualifier of the <code>SAPCpiOutboundConfig.receiverName</code> attribute **/
	public static final String RECEIVERNAME = "receiverName";
	/** Qualifier of the <code>SAPCpiOutboundConfig.receiverPort</code> attribute **/
	public static final String RECEIVERPORT = "receiverPort";
	/** Qualifier of the <code>SAPCpiOutboundConfig.client</code> attribute **/
	public static final String CLIENT = "client";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(URL, AttributeMode.INITIAL);
		tmp.put(USERNAME, AttributeMode.INITIAL);
		tmp.put(SENDERNAME, AttributeMode.INITIAL);
		tmp.put(SENDERPORT, AttributeMode.INITIAL);
		tmp.put(RECEIVERNAME, AttributeMode.INITIAL);
		tmp.put(RECEIVERPORT, AttributeMode.INITIAL);
		tmp.put(CLIENT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.client</code> attribute.
	 * @return the client
	 */
	public String getClient(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CLIENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.client</code> attribute.
	 * @return the client
	 */
	public String getClient()
	{
		return getClient( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.client</code> attribute. 
	 * @param value the client
	 */
	public void setClient(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CLIENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.client</code> attribute. 
	 * @param value the client
	 */
	public void setClient(final String value)
	{
		setClient( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.receiverName</code> attribute.
	 * @return the receiverName
	 */
	public String getReceiverName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RECEIVERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.receiverName</code> attribute.
	 * @return the receiverName
	 */
	public String getReceiverName()
	{
		return getReceiverName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.receiverName</code> attribute. 
	 * @param value the receiverName
	 */
	public void setReceiverName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RECEIVERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.receiverName</code> attribute. 
	 * @param value the receiverName
	 */
	public void setReceiverName(final String value)
	{
		setReceiverName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.receiverPort</code> attribute.
	 * @return the receiverPort
	 */
	public String getReceiverPort(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RECEIVERPORT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.receiverPort</code> attribute.
	 * @return the receiverPort
	 */
	public String getReceiverPort()
	{
		return getReceiverPort( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.receiverPort</code> attribute. 
	 * @param value the receiverPort
	 */
	public void setReceiverPort(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RECEIVERPORT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.receiverPort</code> attribute. 
	 * @param value the receiverPort
	 */
	public void setReceiverPort(final String value)
	{
		setReceiverPort( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.senderName</code> attribute.
	 * @return the senderName
	 */
	public String getSenderName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SENDERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.senderName</code> attribute.
	 * @return the senderName
	 */
	public String getSenderName()
	{
		return getSenderName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.senderName</code> attribute. 
	 * @param value the senderName
	 */
	public void setSenderName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SENDERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.senderName</code> attribute. 
	 * @param value the senderName
	 */
	public void setSenderName(final String value)
	{
		setSenderName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.senderPort</code> attribute.
	 * @return the senderPort
	 */
	public String getSenderPort(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SENDERPORT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.senderPort</code> attribute.
	 * @return the senderPort
	 */
	public String getSenderPort()
	{
		return getSenderPort( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.senderPort</code> attribute. 
	 * @param value the senderPort
	 */
	public void setSenderPort(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SENDERPORT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.senderPort</code> attribute. 
	 * @param value the senderPort
	 */
	public void setSenderPort(final String value)
	{
		setSenderPort( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.url</code> attribute.
	 * @return the url
	 */
	public String getUrl(final SessionContext ctx)
	{
		return (String)getProperty( ctx, URL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.url</code> attribute.
	 * @return the url
	 */
	public String getUrl()
	{
		return getUrl( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.url</code> attribute. 
	 * @param value the url
	 */
	public void setUrl(final SessionContext ctx, final String value)
	{
		setProperty(ctx, URL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.url</code> attribute. 
	 * @param value the url
	 */
	public void setUrl(final String value)
	{
		setUrl( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.username</code> attribute.
	 * @return the username
	 */
	public String getUsername(final SessionContext ctx)
	{
		return (String)getProperty( ctx, USERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundConfig.username</code> attribute.
	 * @return the username
	 */
	public String getUsername()
	{
		return getUsername( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.username</code> attribute. 
	 * @param value the username
	 */
	public void setUsername(final SessionContext ctx, final String value)
	{
		setProperty(ctx, USERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundConfig.username</code> attribute. 
	 * @param value the username
	 */
	public void setUsername(final String value)
	{
		setUsername( getSession().getSessionContext(), value );
	}
	
}
