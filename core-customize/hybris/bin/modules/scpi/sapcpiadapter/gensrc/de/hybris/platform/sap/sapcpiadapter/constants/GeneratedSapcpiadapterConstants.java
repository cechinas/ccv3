/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSapcpiadapterConstants
{
	public static final String EXTENSIONNAME = "sapcpiadapter";
	public static class TC
	{
		public static final String SAPCPIOUTBOUNDADDRESS = "SAPCpiOutboundAddress".intern();
		public static final String SAPCPIOUTBOUNDB2BCONTACT = "SAPCpiOutboundB2BContact".intern();
		public static final String SAPCPIOUTBOUNDB2BCUSTOMER = "SAPCpiOutboundB2BCustomer".intern();
		public static final String SAPCPIOUTBOUNDCARDPAYMENT = "SAPCpiOutboundCardPayment".intern();
		public static final String SAPCPIOUTBOUNDCONFIG = "SAPCpiOutboundConfig".intern();
		public static final String SAPCPIOUTBOUNDCUSTOMER = "SAPCpiOutboundCustomer".intern();
		public static final String SAPCPIOUTBOUNDORDER = "SAPCpiOutboundOrder".intern();
		public static final String SAPCPIOUTBOUNDORDERCANCELLATION = "SAPCpiOutboundOrderCancellation".intern();
		public static final String SAPCPIOUTBOUNDORDERITEM = "SAPCpiOutboundOrderItem".intern();
		public static final String SAPCPIOUTBOUNDPARTNERROLE = "SAPCpiOutboundPartnerRole".intern();
		public static final String SAPCPIOUTBOUNDPRICECOMPONENT = "SAPCpiOutboundPriceComponent".intern();
	}
	public static class Attributes
	{
		public static class Address
		{
			public static final String SAPADDRESSUUID = "sapAddressUUID".intern();
		}
	}
	public static class Relations
	{
		public static final String SAPCPIOUTBOUNDB2BCUSTOMER2SAPCPIOUTBOUNDB2BCONTACT = "SAPCpiOutboundB2BCustomer2SAPCpiOutboundB2BContact".intern();
		public static final String SAPCPIOUTBOUNDORDER2SAPCPIOUTBOUNDADDRESS = "SAPCpiOutboundOrder2SAPCpiOutboundAddress".intern();
		public static final String SAPCPIOUTBOUNDORDER2SAPCPIOUTBOUNDCARDPAYMENT = "SAPCpiOutboundOrder2SAPCpiOutboundCardPayment".intern();
		public static final String SAPCPIOUTBOUNDORDER2SAPCPIOUTBOUNDORDERITEM = "SAPCpiOutboundOrder2SAPCpiOutboundOrderItem".intern();
		public static final String SAPCPIOUTBOUNDORDER2SAPCPIOUTBOUNDPARTNERROLE = "SAPCpiOutboundOrder2SAPCpiOutboundPartnerRole".intern();
		public static final String SAPCPIOUTBOUNDORDER2SAPCPIOUTBOUNDPRICECOMPONENT = "SAPCpiOutboundOrder2SAPCpiOutboundPriceComponent".intern();
	}
	
	protected GeneratedSapcpiadapterConstants()
	{
		// private constructor
	}
	
	
}
