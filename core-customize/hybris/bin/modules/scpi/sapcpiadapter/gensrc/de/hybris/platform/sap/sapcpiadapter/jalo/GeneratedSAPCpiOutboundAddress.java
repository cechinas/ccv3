/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundAddress SAPCpiOutboundAddress}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundAddress extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundAddress.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundAddress.documentAddressId</code> attribute **/
	public static final String DOCUMENTADDRESSID = "documentAddressId";
	/** Qualifier of the <code>SAPCpiOutboundAddress.firstName</code> attribute **/
	public static final String FIRSTNAME = "firstName";
	/** Qualifier of the <code>SAPCpiOutboundAddress.lastName</code> attribute **/
	public static final String LASTNAME = "lastName";
	/** Qualifier of the <code>SAPCpiOutboundAddress.middleName</code> attribute **/
	public static final String MIDDLENAME = "middleName";
	/** Qualifier of the <code>SAPCpiOutboundAddress.middleName2</code> attribute **/
	public static final String MIDDLENAME2 = "middleName2";
	/** Qualifier of the <code>SAPCpiOutboundAddress.street</code> attribute **/
	public static final String STREET = "street";
	/** Qualifier of the <code>SAPCpiOutboundAddress.city</code> attribute **/
	public static final String CITY = "city";
	/** Qualifier of the <code>SAPCpiOutboundAddress.district</code> attribute **/
	public static final String DISTRICT = "district";
	/** Qualifier of the <code>SAPCpiOutboundAddress.building</code> attribute **/
	public static final String BUILDING = "building";
	/** Qualifier of the <code>SAPCpiOutboundAddress.apartment</code> attribute **/
	public static final String APARTMENT = "apartment";
	/** Qualifier of the <code>SAPCpiOutboundAddress.pobox</code> attribute **/
	public static final String POBOX = "pobox";
	/** Qualifier of the <code>SAPCpiOutboundAddress.faxNumber</code> attribute **/
	public static final String FAXNUMBER = "faxNumber";
	/** Qualifier of the <code>SAPCpiOutboundAddress.titleCode</code> attribute **/
	public static final String TITLECODE = "titleCode";
	/** Qualifier of the <code>SAPCpiOutboundAddress.telNumber</code> attribute **/
	public static final String TELNUMBER = "telNumber";
	/** Qualifier of the <code>SAPCpiOutboundAddress.houseNumber</code> attribute **/
	public static final String HOUSENUMBER = "houseNumber";
	/** Qualifier of the <code>SAPCpiOutboundAddress.postalCode</code> attribute **/
	public static final String POSTALCODE = "postalCode";
	/** Qualifier of the <code>SAPCpiOutboundAddress.regionIsoCode</code> attribute **/
	public static final String REGIONISOCODE = "regionIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundAddress.countryIsoCode</code> attribute **/
	public static final String COUNTRYISOCODE = "countryIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundAddress.email</code> attribute **/
	public static final String EMAIL = "email";
	/** Qualifier of the <code>SAPCpiOutboundAddress.languageIsoCode</code> attribute **/
	public static final String LANGUAGEISOCODE = "languageIsoCode";
	/** Qualifier of the <code>SAPCpiOutboundAddress.sapCpiOutboundOrder</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDER = "sapCpiOutboundOrder";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundAddress> SAPCPIOUTBOUNDORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundAddress>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDADDRESS,
	false,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(DOCUMENTADDRESSID, AttributeMode.INITIAL);
		tmp.put(FIRSTNAME, AttributeMode.INITIAL);
		tmp.put(LASTNAME, AttributeMode.INITIAL);
		tmp.put(MIDDLENAME, AttributeMode.INITIAL);
		tmp.put(MIDDLENAME2, AttributeMode.INITIAL);
		tmp.put(STREET, AttributeMode.INITIAL);
		tmp.put(CITY, AttributeMode.INITIAL);
		tmp.put(DISTRICT, AttributeMode.INITIAL);
		tmp.put(BUILDING, AttributeMode.INITIAL);
		tmp.put(APARTMENT, AttributeMode.INITIAL);
		tmp.put(POBOX, AttributeMode.INITIAL);
		tmp.put(FAXNUMBER, AttributeMode.INITIAL);
		tmp.put(TITLECODE, AttributeMode.INITIAL);
		tmp.put(TELNUMBER, AttributeMode.INITIAL);
		tmp.put(HOUSENUMBER, AttributeMode.INITIAL);
		tmp.put(POSTALCODE, AttributeMode.INITIAL);
		tmp.put(REGIONISOCODE, AttributeMode.INITIAL);
		tmp.put(COUNTRYISOCODE, AttributeMode.INITIAL);
		tmp.put(EMAIL, AttributeMode.INITIAL);
		tmp.put(LANGUAGEISOCODE, AttributeMode.INITIAL);
		tmp.put(SAPCPIOUTBOUNDORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.apartment</code> attribute.
	 * @return the apartment
	 */
	public String getApartment(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APARTMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.apartment</code> attribute.
	 * @return the apartment
	 */
	public String getApartment()
	{
		return getApartment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.apartment</code> attribute. 
	 * @param value the apartment
	 */
	public void setApartment(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APARTMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.apartment</code> attribute. 
	 * @param value the apartment
	 */
	public void setApartment(final String value)
	{
		setApartment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.building</code> attribute.
	 * @return the building
	 */
	public String getBuilding(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BUILDING);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.building</code> attribute.
	 * @return the building
	 */
	public String getBuilding()
	{
		return getBuilding( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.building</code> attribute. 
	 * @param value the building
	 */
	public void setBuilding(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BUILDING,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.building</code> attribute. 
	 * @param value the building
	 */
	public void setBuilding(final String value)
	{
		setBuilding( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.city</code> attribute.
	 * @return the city
	 */
	public String getCity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.city</code> attribute.
	 * @return the city
	 */
	public String getCity()
	{
		return getCity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.city</code> attribute. 
	 * @param value the city
	 */
	public void setCity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.city</code> attribute. 
	 * @param value the city
	 */
	public void setCity(final String value)
	{
		setCity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.countryIsoCode</code> attribute.
	 * @return the countryIsoCode
	 */
	public String getCountryIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COUNTRYISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.countryIsoCode</code> attribute.
	 * @return the countryIsoCode
	 */
	public String getCountryIsoCode()
	{
		return getCountryIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.countryIsoCode</code> attribute. 
	 * @param value the countryIsoCode
	 */
	public void setCountryIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COUNTRYISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.countryIsoCode</code> attribute. 
	 * @param value the countryIsoCode
	 */
	public void setCountryIsoCode(final String value)
	{
		setCountryIsoCode( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.district</code> attribute.
	 * @return the district
	 */
	public String getDistrict(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DISTRICT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.district</code> attribute.
	 * @return the district
	 */
	public String getDistrict()
	{
		return getDistrict( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.district</code> attribute. 
	 * @param value the district
	 */
	public void setDistrict(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DISTRICT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.district</code> attribute. 
	 * @param value the district
	 */
	public void setDistrict(final String value)
	{
		setDistrict( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.documentAddressId</code> attribute.
	 * @return the documentAddressId
	 */
	public String getDocumentAddressId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DOCUMENTADDRESSID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.documentAddressId</code> attribute.
	 * @return the documentAddressId
	 */
	public String getDocumentAddressId()
	{
		return getDocumentAddressId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.documentAddressId</code> attribute. 
	 * @param value the documentAddressId
	 */
	public void setDocumentAddressId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DOCUMENTADDRESSID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.documentAddressId</code> attribute. 
	 * @param value the documentAddressId
	 */
	public void setDocumentAddressId(final String value)
	{
		setDocumentAddressId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.email</code> attribute.
	 * @return the email
	 */
	public String getEmail(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.email</code> attribute.
	 * @return the email
	 */
	public String getEmail()
	{
		return getEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final String value)
	{
		setEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.faxNumber</code> attribute.
	 * @return the faxNumber
	 */
	public String getFaxNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAXNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.faxNumber</code> attribute.
	 * @return the faxNumber
	 */
	public String getFaxNumber()
	{
		return getFaxNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.faxNumber</code> attribute. 
	 * @param value the faxNumber
	 */
	public void setFaxNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAXNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.faxNumber</code> attribute. 
	 * @param value the faxNumber
	 */
	public void setFaxNumber(final String value)
	{
		setFaxNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.firstName</code> attribute.
	 * @return the firstName
	 */
	public String getFirstName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FIRSTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.firstName</code> attribute.
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return getFirstName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.firstName</code> attribute. 
	 * @param value the firstName
	 */
	public void setFirstName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FIRSTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.firstName</code> attribute. 
	 * @param value the firstName
	 */
	public void setFirstName(final String value)
	{
		setFirstName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.houseNumber</code> attribute.
	 * @return the houseNumber
	 */
	public String getHouseNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HOUSENUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.houseNumber</code> attribute.
	 * @return the houseNumber
	 */
	public String getHouseNumber()
	{
		return getHouseNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.houseNumber</code> attribute. 
	 * @param value the houseNumber
	 */
	public void setHouseNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HOUSENUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.houseNumber</code> attribute. 
	 * @param value the houseNumber
	 */
	public void setHouseNumber(final String value)
	{
		setHouseNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.languageIsoCode</code> attribute.
	 * @return the languageIsoCode
	 */
	public String getLanguageIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LANGUAGEISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.languageIsoCode</code> attribute.
	 * @return the languageIsoCode
	 */
	public String getLanguageIsoCode()
	{
		return getLanguageIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.languageIsoCode</code> attribute. 
	 * @param value the languageIsoCode
	 */
	public void setLanguageIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LANGUAGEISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.languageIsoCode</code> attribute. 
	 * @param value the languageIsoCode
	 */
	public void setLanguageIsoCode(final String value)
	{
		setLanguageIsoCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.lastName</code> attribute.
	 * @return the lastName
	 */
	public String getLastName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LASTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.lastName</code> attribute.
	 * @return the lastName
	 */
	public String getLastName()
	{
		return getLastName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.lastName</code> attribute. 
	 * @param value the lastName
	 */
	public void setLastName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LASTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.lastName</code> attribute. 
	 * @param value the lastName
	 */
	public void setLastName(final String value)
	{
		setLastName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.middleName</code> attribute.
	 * @return the middleName
	 */
	public String getMiddleName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MIDDLENAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.middleName</code> attribute.
	 * @return the middleName
	 */
	public String getMiddleName()
	{
		return getMiddleName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.middleName</code> attribute. 
	 * @param value the middleName
	 */
	public void setMiddleName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MIDDLENAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.middleName</code> attribute. 
	 * @param value the middleName
	 */
	public void setMiddleName(final String value)
	{
		setMiddleName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.middleName2</code> attribute.
	 * @return the middleName2
	 */
	public String getMiddleName2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MIDDLENAME2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.middleName2</code> attribute.
	 * @return the middleName2
	 */
	public String getMiddleName2()
	{
		return getMiddleName2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.middleName2</code> attribute. 
	 * @param value the middleName2
	 */
	public void setMiddleName2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MIDDLENAME2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.middleName2</code> attribute. 
	 * @param value the middleName2
	 */
	public void setMiddleName2(final String value)
	{
		setMiddleName2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.pobox</code> attribute.
	 * @return the pobox
	 */
	public String getPobox(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POBOX);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.pobox</code> attribute.
	 * @return the pobox
	 */
	public String getPobox()
	{
		return getPobox( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.pobox</code> attribute. 
	 * @param value the pobox
	 */
	public void setPobox(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POBOX,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.pobox</code> attribute. 
	 * @param value the pobox
	 */
	public void setPobox(final String value)
	{
		setPobox( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.postalCode</code> attribute.
	 * @return the postalCode
	 */
	public String getPostalCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POSTALCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.postalCode</code> attribute.
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return getPostalCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.postalCode</code> attribute. 
	 * @param value the postalCode
	 */
	public void setPostalCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POSTALCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.postalCode</code> attribute. 
	 * @param value the postalCode
	 */
	public void setPostalCode(final String value)
	{
		setPostalCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.regionIsoCode</code> attribute.
	 * @return the regionIsoCode
	 */
	public String getRegionIsoCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REGIONISOCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.regionIsoCode</code> attribute.
	 * @return the regionIsoCode
	 */
	public String getRegionIsoCode()
	{
		return getRegionIsoCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.regionIsoCode</code> attribute. 
	 * @param value the regionIsoCode
	 */
	public void setRegionIsoCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REGIONISOCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.regionIsoCode</code> attribute. 
	 * @param value the regionIsoCode
	 */
	public void setRegionIsoCode(final String value)
	{
		setRegionIsoCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder(final SessionContext ctx)
	{
		return (SAPCpiOutboundOrder)getProperty( ctx, SAPCPIOUTBOUNDORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder()
	{
		return getSapCpiOutboundOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SessionContext ctx, final SAPCpiOutboundOrder value)
	{
		SAPCPIOUTBOUNDORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SAPCpiOutboundOrder value)
	{
		setSapCpiOutboundOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.street</code> attribute.
	 * @return the street
	 */
	public String getStreet(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STREET);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.street</code> attribute.
	 * @return the street
	 */
	public String getStreet()
	{
		return getStreet( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.street</code> attribute. 
	 * @param value the street
	 */
	public void setStreet(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STREET,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.street</code> attribute. 
	 * @param value the street
	 */
	public void setStreet(final String value)
	{
		setStreet( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.telNumber</code> attribute.
	 * @return the telNumber
	 */
	public String getTelNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TELNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.telNumber</code> attribute.
	 * @return the telNumber
	 */
	public String getTelNumber()
	{
		return getTelNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.telNumber</code> attribute. 
	 * @param value the telNumber
	 */
	public void setTelNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TELNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.telNumber</code> attribute. 
	 * @param value the telNumber
	 */
	public void setTelNumber(final String value)
	{
		setTelNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.titleCode</code> attribute.
	 * @return the titleCode
	 */
	public String getTitleCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TITLECODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundAddress.titleCode</code> attribute.
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return getTitleCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.titleCode</code> attribute. 
	 * @param value the titleCode
	 */
	public void setTitleCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TITLECODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundAddress.titleCode</code> attribute. 
	 * @param value the titleCode
	 */
	public void setTitleCode(final String value)
	{
		setTitleCode( getSession().getSessionContext(), value );
	}
	
}
