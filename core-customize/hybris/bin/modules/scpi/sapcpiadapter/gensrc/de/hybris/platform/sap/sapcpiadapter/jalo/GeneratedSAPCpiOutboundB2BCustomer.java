/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BContact;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCustomer;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BCustomer SAPCpiOutboundB2BCustomer}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundB2BCustomer extends SAPCpiOutboundCustomer
{
	/** Qualifier of the <code>SAPCpiOutboundB2BCustomer.email</code> attribute **/
	public static final String EMAIL = "email";
	/** Qualifier of the <code>SAPCpiOutboundB2BCustomer.defaultB2BUnit</code> attribute **/
	public static final String DEFAULTB2BUNIT = "defaultB2BUnit";
	/** Qualifier of the <code>SAPCpiOutboundB2BCustomer.groups</code> attribute **/
	public static final String GROUPS = "groups";
	/** Qualifier of the <code>SAPCpiOutboundB2BCustomer.sapCpiOutboundB2BContacts</code> attribute **/
	public static final String SAPCPIOUTBOUNDB2BCONTACTS = "sapCpiOutboundB2BContacts";
	/**
	* {@link OneToManyHandler} for handling 1:n SAPCPIOUTBOUNDB2BCONTACTS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SAPCpiOutboundB2BContact> SAPCPIOUTBOUNDB2BCONTACTSHANDLER = new OneToManyHandler<SAPCpiOutboundB2BContact>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDB2BCONTACT,
	true,
	"sapCpiOutboundB2BCustomer",
	null,
	false,
	true,
	CollectionType.SET
	).withRelationQualifier("sapCpiOutboundB2BContacts");
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SAPCpiOutboundCustomer.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(EMAIL, AttributeMode.INITIAL);
		tmp.put(DEFAULTB2BUNIT, AttributeMode.INITIAL);
		tmp.put(GROUPS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.defaultB2BUnit</code> attribute.
	 * @return the defaultB2BUnit
	 */
	public String getDefaultB2BUnit(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DEFAULTB2BUNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.defaultB2BUnit</code> attribute.
	 * @return the defaultB2BUnit
	 */
	public String getDefaultB2BUnit()
	{
		return getDefaultB2BUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.defaultB2BUnit</code> attribute. 
	 * @param value the defaultB2BUnit
	 */
	public void setDefaultB2BUnit(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DEFAULTB2BUNIT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.defaultB2BUnit</code> attribute. 
	 * @param value the defaultB2BUnit
	 */
	public void setDefaultB2BUnit(final String value)
	{
		setDefaultB2BUnit( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.email</code> attribute.
	 * @return the email
	 */
	public String getEmail(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.email</code> attribute.
	 * @return the email
	 */
	public String getEmail()
	{
		return getEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final String value)
	{
		setEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.groups</code> attribute.
	 * @return the groups
	 */
	public String getGroups(final SessionContext ctx)
	{
		return (String)getProperty( ctx, GROUPS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.groups</code> attribute.
	 * @return the groups
	 */
	public String getGroups()
	{
		return getGroups( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.groups</code> attribute. 
	 * @param value the groups
	 */
	public void setGroups(final SessionContext ctx, final String value)
	{
		setProperty(ctx, GROUPS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.groups</code> attribute. 
	 * @param value the groups
	 */
	public void setGroups(final String value)
	{
		setGroups( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.sapCpiOutboundB2BContacts</code> attribute.
	 * @return the sapCpiOutboundB2BContacts
	 */
	public Set<SAPCpiOutboundB2BContact> getSapCpiOutboundB2BContacts(final SessionContext ctx)
	{
		return (Set<SAPCpiOutboundB2BContact>)SAPCPIOUTBOUNDB2BCONTACTSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundB2BCustomer.sapCpiOutboundB2BContacts</code> attribute.
	 * @return the sapCpiOutboundB2BContacts
	 */
	public Set<SAPCpiOutboundB2BContact> getSapCpiOutboundB2BContacts()
	{
		return getSapCpiOutboundB2BContacts( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.sapCpiOutboundB2BContacts</code> attribute. 
	 * @param value the sapCpiOutboundB2BContacts
	 */
	public void setSapCpiOutboundB2BContacts(final SessionContext ctx, final Set<SAPCpiOutboundB2BContact> value)
	{
		SAPCPIOUTBOUNDB2BCONTACTSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundB2BCustomer.sapCpiOutboundB2BContacts</code> attribute. 
	 * @param value the sapCpiOutboundB2BContacts
	 */
	public void setSapCpiOutboundB2BContacts(final Set<SAPCpiOutboundB2BContact> value)
	{
		setSapCpiOutboundB2BContacts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundB2BContacts. 
	 * @param value the item to add to sapCpiOutboundB2BContacts
	 */
	public void addToSapCpiOutboundB2BContacts(final SessionContext ctx, final SAPCpiOutboundB2BContact value)
	{
		SAPCPIOUTBOUNDB2BCONTACTSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to sapCpiOutboundB2BContacts. 
	 * @param value the item to add to sapCpiOutboundB2BContacts
	 */
	public void addToSapCpiOutboundB2BContacts(final SAPCpiOutboundB2BContact value)
	{
		addToSapCpiOutboundB2BContacts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundB2BContacts. 
	 * @param value the item to remove from sapCpiOutboundB2BContacts
	 */
	public void removeFromSapCpiOutboundB2BContacts(final SessionContext ctx, final SAPCpiOutboundB2BContact value)
	{
		SAPCPIOUTBOUNDB2BCONTACTSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from sapCpiOutboundB2BContacts. 
	 * @param value the item to remove from sapCpiOutboundB2BContacts
	 */
	public void removeFromSapCpiOutboundB2BContacts(final SAPCpiOutboundB2BContact value)
	{
		removeFromSapCpiOutboundB2BContacts( getSession().getSessionContext(), value );
	}
	
}
