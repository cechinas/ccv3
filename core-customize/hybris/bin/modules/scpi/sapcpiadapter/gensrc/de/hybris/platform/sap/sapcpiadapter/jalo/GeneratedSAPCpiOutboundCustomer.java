/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundConfig;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCustomer SAPCpiOutboundCustomer}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundCustomer extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundCustomer.uid</code> attribute **/
	public static final String UID = "uid";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.contactId</code> attribute **/
	public static final String CONTACTID = "contactId";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.customerId</code> attribute **/
	public static final String CUSTOMERID = "customerId";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.firstName</code> attribute **/
	public static final String FIRSTNAME = "firstName";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.lastName</code> attribute **/
	public static final String LASTNAME = "lastName";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.sessionLanguage</code> attribute **/
	public static final String SESSIONLANGUAGE = "sessionLanguage";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.title</code> attribute **/
	public static final String TITLE = "title";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.baseStore</code> attribute **/
	public static final String BASESTORE = "baseStore";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.objType</code> attribute **/
	public static final String OBJTYPE = "objType";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.addressUsage</code> attribute **/
	public static final String ADDRESSUSAGE = "addressUsage";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.addressUUID</code> attribute **/
	public static final String ADDRESSUUID = "addressUUID";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.country</code> attribute **/
	public static final String COUNTRY = "country";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.street</code> attribute **/
	public static final String STREET = "street";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.phone</code> attribute **/
	public static final String PHONE = "phone";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.fax</code> attribute **/
	public static final String FAX = "fax";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.town</code> attribute **/
	public static final String TOWN = "town";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.postalCode</code> attribute **/
	public static final String POSTALCODE = "postalCode";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.streetNumber</code> attribute **/
	public static final String STREETNUMBER = "streetNumber";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.region</code> attribute **/
	public static final String REGION = "region";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.responseStatus</code> attribute **/
	public static final String RESPONSESTATUS = "responseStatus";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.responseMessage</code> attribute **/
	public static final String RESPONSEMESSAGE = "responseMessage";
	/** Qualifier of the <code>SAPCpiOutboundCustomer.sapCpiConfig</code> attribute **/
	public static final String SAPCPICONFIG = "sapCpiConfig";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(UID, AttributeMode.INITIAL);
		tmp.put(CONTACTID, AttributeMode.INITIAL);
		tmp.put(CUSTOMERID, AttributeMode.INITIAL);
		tmp.put(FIRSTNAME, AttributeMode.INITIAL);
		tmp.put(LASTNAME, AttributeMode.INITIAL);
		tmp.put(SESSIONLANGUAGE, AttributeMode.INITIAL);
		tmp.put(TITLE, AttributeMode.INITIAL);
		tmp.put(BASESTORE, AttributeMode.INITIAL);
		tmp.put(OBJTYPE, AttributeMode.INITIAL);
		tmp.put(ADDRESSUSAGE, AttributeMode.INITIAL);
		tmp.put(ADDRESSUUID, AttributeMode.INITIAL);
		tmp.put(COUNTRY, AttributeMode.INITIAL);
		tmp.put(STREET, AttributeMode.INITIAL);
		tmp.put(PHONE, AttributeMode.INITIAL);
		tmp.put(FAX, AttributeMode.INITIAL);
		tmp.put(TOWN, AttributeMode.INITIAL);
		tmp.put(POSTALCODE, AttributeMode.INITIAL);
		tmp.put(STREETNUMBER, AttributeMode.INITIAL);
		tmp.put(REGION, AttributeMode.INITIAL);
		tmp.put(RESPONSESTATUS, AttributeMode.INITIAL);
		tmp.put(RESPONSEMESSAGE, AttributeMode.INITIAL);
		tmp.put(SAPCPICONFIG, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.addressUsage</code> attribute.
	 * @return the addressUsage
	 */
	public String getAddressUsage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ADDRESSUSAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.addressUsage</code> attribute.
	 * @return the addressUsage
	 */
	public String getAddressUsage()
	{
		return getAddressUsage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.addressUsage</code> attribute. 
	 * @param value the addressUsage
	 */
	public void setAddressUsage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ADDRESSUSAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.addressUsage</code> attribute. 
	 * @param value the addressUsage
	 */
	public void setAddressUsage(final String value)
	{
		setAddressUsage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.addressUUID</code> attribute.
	 * @return the addressUUID
	 */
	public String getAddressUUID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ADDRESSUUID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.addressUUID</code> attribute.
	 * @return the addressUUID
	 */
	public String getAddressUUID()
	{
		return getAddressUUID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.addressUUID</code> attribute. 
	 * @param value the addressUUID
	 */
	public void setAddressUUID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ADDRESSUUID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.addressUUID</code> attribute. 
	 * @param value the addressUUID
	 */
	public void setAddressUUID(final String value)
	{
		setAddressUUID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.baseStore</code> attribute.
	 * @return the baseStore
	 */
	public String getBaseStore(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BASESTORE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.baseStore</code> attribute.
	 * @return the baseStore
	 */
	public String getBaseStore()
	{
		return getBaseStore( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.baseStore</code> attribute. 
	 * @param value the baseStore
	 */
	public void setBaseStore(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BASESTORE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.baseStore</code> attribute. 
	 * @param value the baseStore
	 */
	public void setBaseStore(final String value)
	{
		setBaseStore( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.contactId</code> attribute.
	 * @return the contactId
	 */
	public String getContactId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CONTACTID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.contactId</code> attribute.
	 * @return the contactId
	 */
	public String getContactId()
	{
		return getContactId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.contactId</code> attribute. 
	 * @param value the contactId
	 */
	public void setContactId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CONTACTID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.contactId</code> attribute. 
	 * @param value the contactId
	 */
	public void setContactId(final String value)
	{
		setContactId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.country</code> attribute.
	 * @return the country
	 */
	public String getCountry(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COUNTRY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.country</code> attribute.
	 * @return the country
	 */
	public String getCountry()
	{
		return getCountry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.country</code> attribute. 
	 * @param value the country
	 */
	public void setCountry(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COUNTRY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.country</code> attribute. 
	 * @param value the country
	 */
	public void setCountry(final String value)
	{
		setCountry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.customerId</code> attribute.
	 * @return the customerId
	 */
	public String getCustomerId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.customerId</code> attribute.
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return getCustomerId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.customerId</code> attribute. 
	 * @param value the customerId
	 */
	public void setCustomerId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.customerId</code> attribute. 
	 * @param value the customerId
	 */
	public void setCustomerId(final String value)
	{
		setCustomerId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.fax</code> attribute.
	 * @return the fax
	 */
	public String getFax(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAX);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.fax</code> attribute.
	 * @return the fax
	 */
	public String getFax()
	{
		return getFax( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.fax</code> attribute. 
	 * @param value the fax
	 */
	public void setFax(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAX,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.fax</code> attribute. 
	 * @param value the fax
	 */
	public void setFax(final String value)
	{
		setFax( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.firstName</code> attribute.
	 * @return the firstName
	 */
	public String getFirstName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FIRSTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.firstName</code> attribute.
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return getFirstName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.firstName</code> attribute. 
	 * @param value the firstName
	 */
	public void setFirstName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FIRSTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.firstName</code> attribute. 
	 * @param value the firstName
	 */
	public void setFirstName(final String value)
	{
		setFirstName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.lastName</code> attribute.
	 * @return the lastName
	 */
	public String getLastName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LASTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.lastName</code> attribute.
	 * @return the lastName
	 */
	public String getLastName()
	{
		return getLastName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.lastName</code> attribute. 
	 * @param value the lastName
	 */
	public void setLastName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LASTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.lastName</code> attribute. 
	 * @param value the lastName
	 */
	public void setLastName(final String value)
	{
		setLastName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.objType</code> attribute.
	 * @return the objType
	 */
	public String getObjType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, OBJTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.objType</code> attribute.
	 * @return the objType
	 */
	public String getObjType()
	{
		return getObjType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.objType</code> attribute. 
	 * @param value the objType
	 */
	public void setObjType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, OBJTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.objType</code> attribute. 
	 * @param value the objType
	 */
	public void setObjType(final String value)
	{
		setObjType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.phone</code> attribute.
	 * @return the phone
	 */
	public String getPhone(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PHONE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.phone</code> attribute.
	 * @return the phone
	 */
	public String getPhone()
	{
		return getPhone( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.phone</code> attribute. 
	 * @param value the phone
	 */
	public void setPhone(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PHONE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.phone</code> attribute. 
	 * @param value the phone
	 */
	public void setPhone(final String value)
	{
		setPhone( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.postalCode</code> attribute.
	 * @return the postalCode
	 */
	public String getPostalCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POSTALCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.postalCode</code> attribute.
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return getPostalCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.postalCode</code> attribute. 
	 * @param value the postalCode
	 */
	public void setPostalCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POSTALCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.postalCode</code> attribute. 
	 * @param value the postalCode
	 */
	public void setPostalCode(final String value)
	{
		setPostalCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.region</code> attribute.
	 * @return the region
	 */
	public String getRegion(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REGION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.region</code> attribute.
	 * @return the region
	 */
	public String getRegion()
	{
		return getRegion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.region</code> attribute. 
	 * @param value the region
	 */
	public void setRegion(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REGION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.region</code> attribute. 
	 * @param value the region
	 */
	public void setRegion(final String value)
	{
		setRegion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.responseMessage</code> attribute.
	 * @return the responseMessage
	 */
	public String getResponseMessage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RESPONSEMESSAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.responseMessage</code> attribute.
	 * @return the responseMessage
	 */
	public String getResponseMessage()
	{
		return getResponseMessage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.responseMessage</code> attribute. 
	 * @param value the responseMessage
	 */
	public void setResponseMessage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RESPONSEMESSAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.responseMessage</code> attribute. 
	 * @param value the responseMessage
	 */
	public void setResponseMessage(final String value)
	{
		setResponseMessage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.responseStatus</code> attribute.
	 * @return the responseStatus
	 */
	public String getResponseStatus(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RESPONSESTATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.responseStatus</code> attribute.
	 * @return the responseStatus
	 */
	public String getResponseStatus()
	{
		return getResponseStatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.responseStatus</code> attribute. 
	 * @param value the responseStatus
	 */
	public void setResponseStatus(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RESPONSESTATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.responseStatus</code> attribute. 
	 * @param value the responseStatus
	 */
	public void setResponseStatus(final String value)
	{
		setResponseStatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.sapCpiConfig</code> attribute.
	 * @return the sapCpiConfig
	 */
	public SAPCpiOutboundConfig getSapCpiConfig(final SessionContext ctx)
	{
		return (SAPCpiOutboundConfig)getProperty( ctx, SAPCPICONFIG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.sapCpiConfig</code> attribute.
	 * @return the sapCpiConfig
	 */
	public SAPCpiOutboundConfig getSapCpiConfig()
	{
		return getSapCpiConfig( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.sapCpiConfig</code> attribute. 
	 * @param value the sapCpiConfig
	 */
	public void setSapCpiConfig(final SessionContext ctx, final SAPCpiOutboundConfig value)
	{
		setProperty(ctx, SAPCPICONFIG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.sapCpiConfig</code> attribute. 
	 * @param value the sapCpiConfig
	 */
	public void setSapCpiConfig(final SAPCpiOutboundConfig value)
	{
		setSapCpiConfig( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.sessionLanguage</code> attribute.
	 * @return the sessionLanguage
	 */
	public String getSessionLanguage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SESSIONLANGUAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.sessionLanguage</code> attribute.
	 * @return the sessionLanguage
	 */
	public String getSessionLanguage()
	{
		return getSessionLanguage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.sessionLanguage</code> attribute. 
	 * @param value the sessionLanguage
	 */
	public void setSessionLanguage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SESSIONLANGUAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.sessionLanguage</code> attribute. 
	 * @param value the sessionLanguage
	 */
	public void setSessionLanguage(final String value)
	{
		setSessionLanguage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.street</code> attribute.
	 * @return the street
	 */
	public String getStreet(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STREET);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.street</code> attribute.
	 * @return the street
	 */
	public String getStreet()
	{
		return getStreet( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.street</code> attribute. 
	 * @param value the street
	 */
	public void setStreet(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STREET,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.street</code> attribute. 
	 * @param value the street
	 */
	public void setStreet(final String value)
	{
		setStreet( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.streetNumber</code> attribute.
	 * @return the streetNumber
	 */
	public String getStreetNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STREETNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.streetNumber</code> attribute.
	 * @return the streetNumber
	 */
	public String getStreetNumber()
	{
		return getStreetNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.streetNumber</code> attribute. 
	 * @param value the streetNumber
	 */
	public void setStreetNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STREETNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.streetNumber</code> attribute. 
	 * @param value the streetNumber
	 */
	public void setStreetNumber(final String value)
	{
		setStreetNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.title</code> attribute.
	 * @return the title
	 */
	public String getTitle(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TITLE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.title</code> attribute.
	 * @return the title
	 */
	public String getTitle()
	{
		return getTitle( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.title</code> attribute. 
	 * @param value the title
	 */
	public void setTitle(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TITLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.title</code> attribute. 
	 * @param value the title
	 */
	public void setTitle(final String value)
	{
		setTitle( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.town</code> attribute.
	 * @return the town
	 */
	public String getTown(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TOWN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.town</code> attribute.
	 * @return the town
	 */
	public String getTown()
	{
		return getTown( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.town</code> attribute. 
	 * @param value the town
	 */
	public void setTown(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TOWN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.town</code> attribute. 
	 * @param value the town
	 */
	public void setTown(final String value)
	{
		setTown( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.uid</code> attribute.
	 * @return the uid
	 */
	public String getUid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCustomer.uid</code> attribute.
	 * @return the uid
	 */
	public String getUid()
	{
		return getUid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.uid</code> attribute. 
	 * @param value the uid
	 */
	public void setUid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCustomer.uid</code> attribute. 
	 * @param value the uid
	 */
	public void setUid(final String value)
	{
		setUid( getSession().getSessionContext(), value );
	}
	
}
