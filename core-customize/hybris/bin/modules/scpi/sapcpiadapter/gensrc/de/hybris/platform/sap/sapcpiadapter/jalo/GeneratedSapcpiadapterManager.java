/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundAddress;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BContact;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundB2BCustomer;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCardPayment;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundConfig;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCustomer;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrderCancellation;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrderItem;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPartnerRole;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPriceComponent;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>SapcpiadapterManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSapcpiadapterManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("sapAddressUUID", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Address", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public SAPCpiOutboundAddress createSAPCpiOutboundAddress(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDADDRESS );
			return (SAPCpiOutboundAddress)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundAddress : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundAddress createSAPCpiOutboundAddress(final Map attributeValues)
	{
		return createSAPCpiOutboundAddress( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundB2BContact createSAPCpiOutboundB2BContact(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDB2BCONTACT );
			return (SAPCpiOutboundB2BContact)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundB2BContact : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundB2BContact createSAPCpiOutboundB2BContact(final Map attributeValues)
	{
		return createSAPCpiOutboundB2BContact( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundB2BCustomer createSAPCpiOutboundB2BCustomer(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDB2BCUSTOMER );
			return (SAPCpiOutboundB2BCustomer)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundB2BCustomer : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundB2BCustomer createSAPCpiOutboundB2BCustomer(final Map attributeValues)
	{
		return createSAPCpiOutboundB2BCustomer( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundCardPayment createSAPCpiOutboundCardPayment(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDCARDPAYMENT );
			return (SAPCpiOutboundCardPayment)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundCardPayment : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundCardPayment createSAPCpiOutboundCardPayment(final Map attributeValues)
	{
		return createSAPCpiOutboundCardPayment( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundConfig createSAPCpiOutboundConfig(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDCONFIG );
			return (SAPCpiOutboundConfig)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundConfig : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundConfig createSAPCpiOutboundConfig(final Map attributeValues)
	{
		return createSAPCpiOutboundConfig( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundCustomer createSAPCpiOutboundCustomer(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDCUSTOMER );
			return (SAPCpiOutboundCustomer)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundCustomer : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundCustomer createSAPCpiOutboundCustomer(final Map attributeValues)
	{
		return createSAPCpiOutboundCustomer( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundOrder createSAPCpiOutboundOrder(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDORDER );
			return (SAPCpiOutboundOrder)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundOrder : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundOrder createSAPCpiOutboundOrder(final Map attributeValues)
	{
		return createSAPCpiOutboundOrder( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundOrderCancellation createSAPCpiOutboundOrderCancellation(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDORDERCANCELLATION );
			return (SAPCpiOutboundOrderCancellation)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundOrderCancellation : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundOrderCancellation createSAPCpiOutboundOrderCancellation(final Map attributeValues)
	{
		return createSAPCpiOutboundOrderCancellation( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundOrderItem createSAPCpiOutboundOrderItem(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDORDERITEM );
			return (SAPCpiOutboundOrderItem)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundOrderItem : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundOrderItem createSAPCpiOutboundOrderItem(final Map attributeValues)
	{
		return createSAPCpiOutboundOrderItem( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundPartnerRole createSAPCpiOutboundPartnerRole(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPARTNERROLE );
			return (SAPCpiOutboundPartnerRole)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundPartnerRole : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundPartnerRole createSAPCpiOutboundPartnerRole(final Map attributeValues)
	{
		return createSAPCpiOutboundPartnerRole( getSession().getSessionContext(), attributeValues );
	}
	
	public SAPCpiOutboundPriceComponent createSAPCpiOutboundPriceComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPRICECOMPONENT );
			return (SAPCpiOutboundPriceComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SAPCpiOutboundPriceComponent : "+e.getMessage(), 0 );
		}
	}
	
	public SAPCpiOutboundPriceComponent createSAPCpiOutboundPriceComponent(final Map attributeValues)
	{
		return createSAPCpiOutboundPriceComponent( getSession().getSessionContext(), attributeValues );
	}
	
	@Override
	public String getName()
	{
		return SapcpiadapterConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.sapAddressUUID</code> attribute.
	 * @return the sapAddressUUID - SAP Address UUID
	 */
	public String getSapAddressUUID(final SessionContext ctx, final Address item)
	{
		return (String)item.getProperty( ctx, SapcpiadapterConstants.Attributes.Address.SAPADDRESSUUID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.sapAddressUUID</code> attribute.
	 * @return the sapAddressUUID - SAP Address UUID
	 */
	public String getSapAddressUUID(final Address item)
	{
		return getSapAddressUUID( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.sapAddressUUID</code> attribute. 
	 * @param value the sapAddressUUID - SAP Address UUID
	 */
	public void setSapAddressUUID(final SessionContext ctx, final Address item, final String value)
	{
		item.setProperty(ctx, SapcpiadapterConstants.Attributes.Address.SAPADDRESSUUID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.sapAddressUUID</code> attribute. 
	 * @param value the sapAddressUUID - SAP Address UUID
	 */
	public void setSapAddressUUID(final Address item, final String value)
	{
		setSapAddressUUID( getSession().getSessionContext(), item, value );
	}
	
}
