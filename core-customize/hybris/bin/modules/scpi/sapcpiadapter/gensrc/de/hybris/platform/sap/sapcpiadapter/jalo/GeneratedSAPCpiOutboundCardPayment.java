/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundCardPayment SAPCpiOutboundCardPayment}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundCardPayment extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.requestId</code> attribute **/
	public static final String REQUESTID = "requestId";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.ccOwner</code> attribute **/
	public static final String CCOWNER = "ccOwner";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.validToMonth</code> attribute **/
	public static final String VALIDTOMONTH = "validToMonth";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.validToYear</code> attribute **/
	public static final String VALIDTOYEAR = "validToYear";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.subscriptionId</code> attribute **/
	public static final String SUBSCRIPTIONID = "subscriptionId";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.paymentProvider</code> attribute **/
	public static final String PAYMENTPROVIDER = "paymentProvider";
	/** Qualifier of the <code>SAPCpiOutboundCardPayment.sapCpiOutboundOrder</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDER = "sapCpiOutboundOrder";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundCardPayment> SAPCPIOUTBOUNDORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundCardPayment>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDCARDPAYMENT,
	false,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(REQUESTID, AttributeMode.INITIAL);
		tmp.put(CCOWNER, AttributeMode.INITIAL);
		tmp.put(VALIDTOMONTH, AttributeMode.INITIAL);
		tmp.put(VALIDTOYEAR, AttributeMode.INITIAL);
		tmp.put(SUBSCRIPTIONID, AttributeMode.INITIAL);
		tmp.put(PAYMENTPROVIDER, AttributeMode.INITIAL);
		tmp.put(SAPCPIOUTBOUNDORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.ccOwner</code> attribute.
	 * @return the ccOwner
	 */
	public String getCcOwner(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CCOWNER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.ccOwner</code> attribute.
	 * @return the ccOwner
	 */
	public String getCcOwner()
	{
		return getCcOwner( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.ccOwner</code> attribute. 
	 * @param value the ccOwner
	 */
	public void setCcOwner(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CCOWNER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.ccOwner</code> attribute. 
	 * @param value the ccOwner
	 */
	public void setCcOwner(final String value)
	{
		setCcOwner( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.paymentProvider</code> attribute.
	 * @return the paymentProvider
	 */
	public String getPaymentProvider(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PAYMENTPROVIDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.paymentProvider</code> attribute.
	 * @return the paymentProvider
	 */
	public String getPaymentProvider()
	{
		return getPaymentProvider( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.paymentProvider</code> attribute. 
	 * @param value the paymentProvider
	 */
	public void setPaymentProvider(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PAYMENTPROVIDER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.paymentProvider</code> attribute. 
	 * @param value the paymentProvider
	 */
	public void setPaymentProvider(final String value)
	{
		setPaymentProvider( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.requestId</code> attribute.
	 * @return the requestId
	 */
	public String getRequestId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REQUESTID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.requestId</code> attribute.
	 * @return the requestId
	 */
	public String getRequestId()
	{
		return getRequestId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.requestId</code> attribute. 
	 * @param value the requestId
	 */
	public void setRequestId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REQUESTID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.requestId</code> attribute. 
	 * @param value the requestId
	 */
	public void setRequestId(final String value)
	{
		setRequestId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder(final SessionContext ctx)
	{
		return (SAPCpiOutboundOrder)getProperty( ctx, SAPCPIOUTBOUNDORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder()
	{
		return getSapCpiOutboundOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SessionContext ctx, final SAPCpiOutboundOrder value)
	{
		SAPCPIOUTBOUNDORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SAPCpiOutboundOrder value)
	{
		setSapCpiOutboundOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.subscriptionId</code> attribute.
	 * @return the subscriptionId
	 */
	public String getSubscriptionId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUBSCRIPTIONID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.subscriptionId</code> attribute.
	 * @return the subscriptionId
	 */
	public String getSubscriptionId()
	{
		return getSubscriptionId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.subscriptionId</code> attribute. 
	 * @param value the subscriptionId
	 */
	public void setSubscriptionId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUBSCRIPTIONID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.subscriptionId</code> attribute. 
	 * @param value the subscriptionId
	 */
	public void setSubscriptionId(final String value)
	{
		setSubscriptionId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.validToMonth</code> attribute.
	 * @return the validToMonth
	 */
	public String getValidToMonth(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VALIDTOMONTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.validToMonth</code> attribute.
	 * @return the validToMonth
	 */
	public String getValidToMonth()
	{
		return getValidToMonth( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.validToMonth</code> attribute. 
	 * @param value the validToMonth
	 */
	public void setValidToMonth(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VALIDTOMONTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.validToMonth</code> attribute. 
	 * @param value the validToMonth
	 */
	public void setValidToMonth(final String value)
	{
		setValidToMonth( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.validToYear</code> attribute.
	 * @return the validToYear
	 */
	public String getValidToYear(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VALIDTOYEAR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundCardPayment.validToYear</code> attribute.
	 * @return the validToYear
	 */
	public String getValidToYear()
	{
		return getValidToYear( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.validToYear</code> attribute. 
	 * @param value the validToYear
	 */
	public void setValidToYear(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VALIDTOYEAR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundCardPayment.validToYear</code> attribute. 
	 * @param value the validToYear
	 */
	public void setValidToYear(final String value)
	{
		setValidToYear( getSession().getSessionContext(), value );
	}
	
}
