/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiadapter.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.sap.sapcpiadapter.constants.SapcpiadapterConstants;
import de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundOrder;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.sap.sapcpiadapter.jalo.SAPCpiOutboundPartnerRole SAPCpiOutboundPartnerRole}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSAPCpiOutboundPartnerRole extends GenericItem
{
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.entryNumber</code> attribute **/
	public static final String ENTRYNUMBER = "entryNumber";
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.partnerRoleCode</code> attribute **/
	public static final String PARTNERROLECODE = "partnerRoleCode";
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.partnerId</code> attribute **/
	public static final String PARTNERID = "partnerId";
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.documentAddressId</code> attribute **/
	public static final String DOCUMENTADDRESSID = "documentAddressId";
	/** Qualifier of the <code>SAPCpiOutboundPartnerRole.sapCpiOutboundOrder</code> attribute **/
	public static final String SAPCPIOUTBOUNDORDER = "sapCpiOutboundOrder";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n SAPCPIOUTBOUNDORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundPartnerRole> SAPCPIOUTBOUNDORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedSAPCpiOutboundPartnerRole>(
	SapcpiadapterConstants.TC.SAPCPIOUTBOUNDPARTNERROLE,
	false,
	"sapCpiOutboundOrder",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(ENTRYNUMBER, AttributeMode.INITIAL);
		tmp.put(PARTNERROLECODE, AttributeMode.INITIAL);
		tmp.put(PARTNERID, AttributeMode.INITIAL);
		tmp.put(DOCUMENTADDRESSID, AttributeMode.INITIAL);
		tmp.put(SAPCPIOUTBOUNDORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		SAPCPIOUTBOUNDORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.documentAddressId</code> attribute.
	 * @return the documentAddressId
	 */
	public String getDocumentAddressId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DOCUMENTADDRESSID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.documentAddressId</code> attribute.
	 * @return the documentAddressId
	 */
	public String getDocumentAddressId()
	{
		return getDocumentAddressId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.documentAddressId</code> attribute. 
	 * @param value the documentAddressId
	 */
	public void setDocumentAddressId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DOCUMENTADDRESSID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.documentAddressId</code> attribute. 
	 * @param value the documentAddressId
	 */
	public void setDocumentAddressId(final String value)
	{
		setDocumentAddressId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENTRYNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.entryNumber</code> attribute.
	 * @return the entryNumber
	 */
	public String getEntryNumber()
	{
		return getEntryNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENTRYNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.entryNumber</code> attribute. 
	 * @param value the entryNumber
	 */
	public void setEntryNumber(final String value)
	{
		setEntryNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.partnerId</code> attribute.
	 * @return the partnerId
	 */
	public String getPartnerId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.partnerId</code> attribute.
	 * @return the partnerId
	 */
	public String getPartnerId()
	{
		return getPartnerId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.partnerId</code> attribute. 
	 * @param value the partnerId
	 */
	public void setPartnerId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.partnerId</code> attribute. 
	 * @param value the partnerId
	 */
	public void setPartnerId(final String value)
	{
		setPartnerId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.partnerRoleCode</code> attribute.
	 * @return the partnerRoleCode
	 */
	public String getPartnerRoleCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNERROLECODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.partnerRoleCode</code> attribute.
	 * @return the partnerRoleCode
	 */
	public String getPartnerRoleCode()
	{
		return getPartnerRoleCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.partnerRoleCode</code> attribute. 
	 * @param value the partnerRoleCode
	 */
	public void setPartnerRoleCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNERROLECODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.partnerRoleCode</code> attribute. 
	 * @param value the partnerRoleCode
	 */
	public void setPartnerRoleCode(final String value)
	{
		setPartnerRoleCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder(final SessionContext ctx)
	{
		return (SAPCpiOutboundOrder)getProperty( ctx, SAPCPIOUTBOUNDORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SAPCpiOutboundPartnerRole.sapCpiOutboundOrder</code> attribute.
	 * @return the sapCpiOutboundOrder
	 */
	public SAPCpiOutboundOrder getSapCpiOutboundOrder()
	{
		return getSapCpiOutboundOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SessionContext ctx, final SAPCpiOutboundOrder value)
	{
		SAPCPIOUTBOUNDORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SAPCpiOutboundPartnerRole.sapCpiOutboundOrder</code> attribute. 
	 * @param value the sapCpiOutboundOrder
	 */
	public void setSapCpiOutboundOrder(final SAPCpiOutboundOrder value)
	{
		setSapCpiOutboundOrder( getSession().getSessionContext(), value );
	}
	
}
