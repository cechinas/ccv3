/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiorderexchange.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.sap.sapcpiorderexchange.constants.SapcpiorderexchangeConstants;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>SapcpiorderexchangeManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedSapcpiorderexchangeManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("sapPlantCode", AttributeMode.INITIAL);
		tmp.put("sapRejectionReason", AttributeMode.INITIAL);
		tmp.put("sapGoodsIssueDate", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.Order", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	@Override
	public String getName()
	{
		return SapcpiorderexchangeConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapGoodsIssueDate</code> attribute.
	 * @return the sapGoodsIssueDate
	 */
	public Date getSapGoodsIssueDate(final SessionContext ctx, final Order item)
	{
		return (Date)item.getProperty( ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPGOODSISSUEDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapGoodsIssueDate</code> attribute.
	 * @return the sapGoodsIssueDate
	 */
	public Date getSapGoodsIssueDate(final Order item)
	{
		return getSapGoodsIssueDate( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapGoodsIssueDate</code> attribute. 
	 * @param value the sapGoodsIssueDate
	 */
	public void setSapGoodsIssueDate(final SessionContext ctx, final Order item, final Date value)
	{
		item.setProperty(ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPGOODSISSUEDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapGoodsIssueDate</code> attribute. 
	 * @param value the sapGoodsIssueDate
	 */
	public void setSapGoodsIssueDate(final Order item, final Date value)
	{
		setSapGoodsIssueDate( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapPlantCode</code> attribute.
	 * @return the sapPlantCode
	 */
	public String getSapPlantCode(final SessionContext ctx, final Order item)
	{
		return (String)item.getProperty( ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPPLANTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapPlantCode</code> attribute.
	 * @return the sapPlantCode
	 */
	public String getSapPlantCode(final Order item)
	{
		return getSapPlantCode( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapPlantCode</code> attribute. 
	 * @param value the sapPlantCode
	 */
	public void setSapPlantCode(final SessionContext ctx, final Order item, final String value)
	{
		item.setProperty(ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPPLANTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapPlantCode</code> attribute. 
	 * @param value the sapPlantCode
	 */
	public void setSapPlantCode(final Order item, final String value)
	{
		setSapPlantCode( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapRejectionReason</code> attribute.
	 * @return the sapRejectionReason
	 */
	public String getSapRejectionReason(final SessionContext ctx, final Order item)
	{
		return (String)item.getProperty( ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPREJECTIONREASON);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Order.sapRejectionReason</code> attribute.
	 * @return the sapRejectionReason
	 */
	public String getSapRejectionReason(final Order item)
	{
		return getSapRejectionReason( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapRejectionReason</code> attribute. 
	 * @param value the sapRejectionReason
	 */
	public void setSapRejectionReason(final SessionContext ctx, final Order item, final String value)
	{
		item.setProperty(ctx, SapcpiorderexchangeConstants.Attributes.Order.SAPREJECTIONREASON,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Order.sapRejectionReason</code> attribute. 
	 * @param value the sapRejectionReason
	 */
	public void setSapRejectionReason(final Order item, final String value)
	{
		setSapRejectionReason( getSession().getSessionContext(), item, value );
	}
	
}
