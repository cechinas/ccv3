/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapcpiorderexchange.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSapcpiorderexchangeConstants
{
	public static final String EXTENSIONNAME = "sapcpiorderexchange";
	public static class Attributes
	{
		public static class Order
		{
			public static final String SAPGOODSISSUEDATE = "sapGoodsIssueDate".intern();
			public static final String SAPPLANTCODE = "sapPlantCode".intern();
			public static final String SAPREJECTIONREASON = "sapRejectionReason".intern();
		}
	}
	
	protected GeneratedSapcpiorderexchangeConstants()
	{
		// private constructor
	}
	
	
}
