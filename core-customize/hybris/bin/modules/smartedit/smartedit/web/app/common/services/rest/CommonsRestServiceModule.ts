/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
import {SeModule} from 'smarteditcommons/di';
import {PermissionsRestService} from "./PermissionsRestService";

@SeModule({
	providers: [
		PermissionsRestService
	]
})
export class CommonsRestServiceModule {}