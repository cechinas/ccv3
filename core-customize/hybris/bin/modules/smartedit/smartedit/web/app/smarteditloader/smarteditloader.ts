/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
/* forbiddenNameSpaces useClass:false */
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {UpgradeModule} from '@angular/upgrade/static';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {HttpClientModule} from '@angular/common/http';
import {commonNgZone, moduleUtils, nodeUtils, SeTranslationModule, SmarteditCommonsModule, SmarteditErrorHandler, SMARTEDITLOADER_COMPONENT_NAME} from 'smarteditcommons';
import {SmarteditloaderComponent} from './SmarteditloaderComponent';
import {StorageService} from 'smarteditcontainer/services/StorageServiceOuter';
import {TranslationsFetchService} from 'smarteditcontainer/services/http/TranslationsFetchServiceOuter';

@NgModule({
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	imports: [
		BrowserModule,
		HttpClientModule,
		UpgradeModule,
		SmarteditCommonsModule,
		SeTranslationModule.forRoot(TranslationsFetchService, StorageService),
	],
	providers: [
		{
			provide: ErrorHandler,
			useClass: SmarteditErrorHandler,
		},
		StorageService,
		moduleUtils.initialize(() => {
			//
		})
	],
	declarations: [SmarteditloaderComponent],
	entryComponents: [SmarteditloaderComponent],
	bootstrap: [SmarteditloaderComponent]
})
export class Smarteditloader {}

window.smarteditJQuery(document).ready(() => {
	if (!nodeUtils.hasLegacyAngularJSBootsrap()) {
		if (!document.querySelector(SMARTEDITLOADER_COMPONENT_NAME)) {
			document.body.appendChild(document.createElement(SMARTEDITLOADER_COMPONENT_NAME));
		}

		platformBrowserDynamic().bootstrapModule(Smarteditloader, {ngZone: commonNgZone}).catch((err) => console.log(err));
	}

});