/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsoccaddon.redirect.suppliers.impl;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cmsoccaddon.constants.CmsoccaddonConstants;
import de.hybris.platform.cmsoccaddon.data.RequestParamData;
import de.hybris.platform.cmsoccaddon.redirect.suppliers.PageRedirectSupplier;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * Implementation of {@link PageRedirectSupplier} to handle {@link de.hybris.platform.cms2.model.pages.ContentPageModel}
 */
public class ContentPageRedirectSupplier extends AbstractPageRedirectSupplier
{

	@Override
	public boolean shouldRedirect(final HttpServletRequest request, final PreviewDataModel previewData)
	{
		if (Objects.nonNull(previewData.getPage()))
		{
			final String pageLabelOrId = request.getParameter(CmsoccaddonConstants.PAGE_LABEL_ID);
			final ContentPageModel previewPage = (ContentPageModel) previewData.getPage();

			return !(previewPage.getUid().equals(pageLabelOrId)
					|| (Objects.nonNull(previewPage.getLabel()) && previewPage.getLabel().equals(pageLabelOrId)));
		}
		return false;
	}

	@Override
	public void populateParams(final PreviewDataModel previewData, final RequestParamData paramData)
	{
		final ContentPageModel page = (ContentPageModel) previewData.getPage();
		if (Objects.nonNull(page))
		{
			final MultiValueMap<String, String> queryParameters = (MultiValueMap<String, String>) paramData.getQueryParameters();
			queryParameters.set(CmsoccaddonConstants.PAGE_LABEL_ID, page.getLabel());
			queryParameters.set(CmsoccaddonConstants.PAGE_TYPE, ContentPageModel._TYPECODE);
		}
	}

	@Override
	public String getRedirectUrl(final HttpServletRequest request, final PreviewDataModel previewData)
	{
		final RequestParamData paramData = buildRequestParamData(request, previewData);
		return UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString()) //
				.queryParams((MultiValueMap<String, String>) paramData.getQueryParameters()) //
				.build() //
				.toString();
	}

	@Override
	protected String getPreviewCode(final PreviewDataModel previewData)
	{
		throw new UnsupportedOperationException("Preview code is not supported for pages of type " + ContentPageModel._TYPECODE);
	}

}
