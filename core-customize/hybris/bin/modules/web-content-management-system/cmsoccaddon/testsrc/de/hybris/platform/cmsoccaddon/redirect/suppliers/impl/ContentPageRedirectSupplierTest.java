/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsoccaddon.redirect.suppliers.impl;

import static de.hybris.platform.cmsoccaddon.constants.CmsoccaddonConstants.CODE;
import static de.hybris.platform.cmsoccaddon.constants.CmsoccaddonConstants.PAGE_LABEL_ID;
import static de.hybris.platform.cmsoccaddon.constants.CmsoccaddonConstants.PAGE_TYPE;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cmsoccaddon.data.RequestParamData;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ContentPageRedirectSupplierTest
{
	private static final String CONTENT_PAGE_ID = "mockContentPage";
	private static final String CONTENT_PAGE_LABEL = "mockLabel";
	private static final String OTHER_CONTENT_PAGE_ID = "otherMockContentPage";

	@InjectMocks
	private ContentPageRedirectSupplier supplier;

	@Mock
	private HttpServletRequest request;
	@Mock
	private PreviewDataModel previewData;
	@Mock
	private ContentPageModel contentPage;

	@Before
	public void setup()
	{
		when(previewData.getPage()).thenReturn(contentPage);
		when(request.getParameter(PAGE_LABEL_ID)).thenReturn(OTHER_CONTENT_PAGE_ID);
		when(contentPage.getUid()).thenReturn(CONTENT_PAGE_ID);
		when(contentPage.getLabel()).thenReturn(CONTENT_PAGE_LABEL);
		when(contentPage.getItemtype()).thenReturn(ContentPageModel._TYPECODE);
	}

	@Test
	public void shouldRedirectWhenPreviewPageIdNotEqualToRequestedPageId()
	{
		// WHEN
		final boolean value = supplier.shouldRedirect(request, previewData);

		// THEN
		assertTrue(value);
	}

	@Test
	public void shouldNotRedirectWhenPreviewPageIdEaualsRequestedPageId()
	{
		// GIVEN
		when(request.getParameter(PAGE_LABEL_ID)).thenReturn(CONTENT_PAGE_ID);

		// WHEN
		final boolean value = supplier.shouldRedirect(request, previewData);

		// THEN
		assertFalse(value);
	}

	@Test
	public void shouldNotRedirectWhenPreviewPageIsNull()
	{
		// GIVEN
		when(previewData.getPage()).thenReturn(null);

		// WHEN
		final boolean value = supplier.shouldRedirect(request, previewData);

		// THEN
		assertFalse(value);
	}

	@Test
	public void shouldPopulateAllParams()
	{
		final RequestParamData paramData = new RequestParamData();
		paramData.setPathParameters(Collections.emptyMap());
		paramData.setQueryParameters(new LinkedMultiValueMap<>());

		// WHEN
		supplier.populateParams(previewData, paramData);

		// THEN
		assertThat(paramData.getQueryParameters().get(PAGE_LABEL_ID), hasItem(CONTENT_PAGE_LABEL));
		assertThat(paramData.getQueryParameters().get(PAGE_TYPE), hasItem(ContentPageModel._TYPECODE));
		assertThat(paramData.getQueryParameters().get(CODE), nullValue());
		assertThat(paramData.getPathParameters().entrySet(), is(empty()));
	}
}
