/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 */
package yaddonpackage.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedYAddonConstants
{
	public static final String EXTENSIONNAME = "yaddon";
	
	protected GeneratedYAddonConstants()
	{
		// private constructor
	}
	
	
}
