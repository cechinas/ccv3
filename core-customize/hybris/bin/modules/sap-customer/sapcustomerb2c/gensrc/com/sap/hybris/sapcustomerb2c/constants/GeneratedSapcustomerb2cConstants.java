/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 */
package com.sap.hybris.sapcustomerb2c.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSapcustomerb2cConstants
{
	public static final String EXTENSIONNAME = "sapcustomerb2c";
	
	protected GeneratedSapcustomerb2cConstants()
	{
		// private constructor
	}
	
	
}
