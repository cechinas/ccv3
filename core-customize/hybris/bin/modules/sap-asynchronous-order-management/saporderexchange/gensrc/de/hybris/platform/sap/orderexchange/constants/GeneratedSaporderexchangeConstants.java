/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 10 feb 2020 15:41:02                        ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.orderexchange.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSaporderexchangeConstants
{
	public static final String EXTENSIONNAME = "saporderexchange";
	public static class Attributes
	{
		public static class AbstractRule
		{
			public static final String SAPCONDITIONTYPE = "sapConditionType".intern();
		}
		public static class OrderProcess
		{
			public static final String SENDORDERRETRYCOUNT = "sendOrderRetryCount".intern();
		}
		public static class SAPConfiguration
		{
			public static final String SAPORDEREXCHANGE_DELIVERYCOSTCONDITIONTYPE = "saporderexchange_deliveryCostConditionType".intern();
			public static final String SAPORDEREXCHANGE_ITEMPRICECONDITIONTYPE = "saporderexchange_itemPriceConditionType".intern();
			public static final String SAPORDEREXCHANGE_PAYMENTCOSTCONDITIONTYPE = "saporderexchange_paymentCostConditionType".intern();
		}
	}
	
	protected GeneratedSaporderexchangeConstants()
	{
		// private constructor
	}
	
	
}
