/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */

package de.hybris.platform.mediaconversion.conversion;

import static org.assertj.core.api.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.mediaconversion.conversion.ImageMagickSecurityService.ConvertCommandValidationResult;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

@UnitTest
public class ImageMagickSecurityServiceTest
{

	private final ImageMagickSecurityService service = new ImageMagickSecurityService()
	{
		@Override
		String getBlacklistedCommandsFromConfig()
		{
			return "-foo,-bar,-baz,-boom";
		}
	};

	@Test
	public void shouldConsiderCommandValidIfItIsEmpty()
	{
		// given
		final String command = "  ";

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isTrue();
	}

	@Test
	public void shouldConsiderCommandValidIfItContainsEmptyElementOnly()
	{
		// given
		final List<String> command = ImmutableList.of("  ");

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isTrue();
	}

	@Test
	public void shouldConsiderCommandValidIfItIsEmptyList()
	{
		// given
		final List<String> command = Collections.emptyList();

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isTrue();
	}

	@Test
	public void shouldConsiderCommandValidIfItDoesNotContainBlacklistedWords()
	{
		// given
		final String command = "nice command, it ain't gonna offend anyone ;)";

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isTrue();
	}

	@Test
	public void shouldConsiderCommandValidIfListDoesNotContainBlacklistedWords()
	{
		// given
		final List<String> command = ImmutableList.of("nice", "command", "it", "ain't", "gonna", "offend", "anyone ;)");

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isTrue();
	}

	@Test
	public void shouldConsiderCommandInvalidIfItContainsBlacklistedWords()
	{
		// given
		final String command = "something -foo really -bar wrong";

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isFalse();
	}

	@Test
	public void shouldConsiderCommandInvalidIfListContainsBlacklistedWords()
	{
		// given
		final List<String> command = ImmutableList.of("something", "-foo", "really", "-bar", "wrong");

		// when
		final ConvertCommandValidationResult result = service.isCommandSecure(command);

		// then
		assertThat(result).isNotNull();
		assertThat(result.isSecure()).isFalse();
	}
}
