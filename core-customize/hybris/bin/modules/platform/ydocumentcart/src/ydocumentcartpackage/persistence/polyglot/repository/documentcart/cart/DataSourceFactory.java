/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2019 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package ydocumentcartpackage.persistence.polyglot.repository.documentcart.cart;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Objects;

import javax.sql.DataSource;

import org.apache.commons.configuration.Configuration;
import org.springframework.beans.factory.FactoryBean;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public class DataSourceFactory implements FactoryBean<DataSource>
{
	private final ConfigurationService configurationService;

	public DataSourceFactory(final ConfigurationService configurationService)
	{
		this.configurationService = Objects.requireNonNull(configurationService, "configurationService mustn't be null.");
	}

	@Override
	public DataSource getObject() throws Exception
	{
		final Configuration config = configurationService.getConfiguration();
		final HikariConfig poolConfig = new HikariConfig();

		poolConfig.setDriverClassName(config.getString("ydocumentcart.storage.jdbc.driver"));
		poolConfig.setJdbcUrl(config.getString("ydocumentcart.storage.jdbc.url"));
		poolConfig.setUsername(config.getString("ydocumentcart.storage.jdbc.user"));
		poolConfig.setPassword(config.getString("ydocumentcart.storage.jdbc.password"));
		final String propsPrefix = "ydocumentcart.storage.jdbc.props";

		config.getKeys("ydocumentcart.storage.jdbc.props").forEachRemaining(key -> {
			if (key.length() <= propsPrefix.length())
			{
				return;
			}
			final String propertyName = key.substring(propsPrefix.length() + 1);
			poolConfig.addDataSourceProperty(propertyName, config.getString(key));
		});


		return new HikariDataSource(poolConfig);
	}

	@Override
	public Class<?> getObjectType()
	{
		return DataSource.class;
	}
}
